package com.thv.conversation.languageActivity;

import android.content.Context;

import com.thv.conversation.util.objects.Language;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/26/16.
 */

public class LanguagePresenter implements LanguageInterface.RequirePresenterOps {
    LanguageInterface.RequireViewOps mView;
    LanguageModel mModel;
    int curentSelectLanguage = 0;

    public LanguagePresenter(Context context, LanguageInterface.RequireViewOps mView) {
        this.mView = mView;
        mModel = new LanguageModel(context, this);
    }

    void getAllLanguages() {
        mModel.getAllLanguages();
    }

    @Override
    public void loadDataFinish(List<Language> languages, int currentSelect) {
        curentSelectLanguage = currentSelect;
        mView.loadDataFinish(languages, currentSelect);
    }

    public void resetLanguage(int selectLanguage) {
        if (curentSelectLanguage == selectLanguage) {
            mView.finishActivity();
        } else {
            mModel.saveLanguageSelect(selectLanguage);
            mView.resetApplication();
        }

    }
}
