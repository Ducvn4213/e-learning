package com.thv.conversation.yourWordActivity;

import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public class YourWordPresenter {

    YourWordInterfaces mView;
    YourWordModle mModle;

    public YourWordPresenter(YourWordActivity mView) {
        this.mView = mView;
        mModle = new YourWordModle(mView);
    }


    public void getAllWordBookmark() {
        List<LessonDetail> lessonDetails = mModle.getAllWordBookmark();
        mView.getAllWordBookmark(lessonDetails);
    }

    public void saveLessonDetail(LessonDetail lessonDetail) {
        mModle.saveLessonDetail(lessonDetail);
    }

    public void bookmarkVocabulary(LessonDetail lessonDetail) {
        lessonDetail.bookMarkLesson();
        mModle.bookmarkVocabulary(lessonDetail);
    }


    public void removeBookmarkVocabulary(LessonDetail lessonDetail) {
        lessonDetail.removeBookMark();
        mModle.bookmarkVocabulary(lessonDetail);
    }


    public void bookMarkAllVocabulary(List<LessonDetail> listLessonDetail) {
        for (int i = 0; i < listLessonDetail.size(); i++) {
            bookmarkVocabulary((listLessonDetail.get(i)));
        }
    }

    public int checkCorrectAnswer(LessonDetail lessonDetail, List<String> results) {
        int currentStar = lessonDetail.getStar();
        lessonDetail.checkCorrect(results);
        if (currentStar < lessonDetail.getStar()) {
            saveLessonDetail(lessonDetail);
            return lessonDetail.getStar();
        }
        return 0;
    }

    public void removeAllBookmark() {
        mModle.removeAllBookmark();
    }

    public LessonDetail saveNewWord(String word, String meaning) {
        LessonDetail lessonDetail = null;
        if (word.length() > 0 && meaning.length() > 0) {
            lessonDetail = new LessonDetail(word, meaning);
            int idLesson = mModle.saveNewWord(lessonDetail);
            if (idLesson == 0) return null;
            lessonDetail.setId(idLesson);
        }
        return lessonDetail;
    }
}
