package com.thv.conversation.tab2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thv.conversation.R;
import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.adapters.LessonAdapter;
import com.thv.conversation.util.dialog.DialogMessage;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.vocabularyActivity.VocabularyActivity;

import java.util.List;

public class Tab2 extends Fragment implements Tab2Interfaces, LessonAdapter.OnItemClickListener {
    private static final int VOCABULARY_ACTIVITY = 2;
    private Tab2Presenter tab2Presenter;
    private RecyclerView rvLessson;
    private LessonAdapter lessonAdapter;
    private List<Lesson> lessonsConversation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_1, container, false);
        bindingControl(view);
        if (lessonsConversation == null) {
            getLesson();
        } else {
            setupUI();
        }
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void bindingControl(View view) {
        rvLessson = (RecyclerView) view.findViewById(R.id.recyclerview_lessson);
    }

    public void updateFromActivity() {
        if (getView() != null) {
            getLesson();
        }
    }

    public void getLesson() {
        tab2Presenter = new Tab2Presenter(getActivity(), this);
        tab2Presenter.getLessons();
    }


    public void setupUI() {
        rvLessson.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvLessson.setLayoutManager(mLayoutManager);
        lessonAdapter = new LessonAdapter(lessonsConversation, this);
        rvLessson.setAdapter(lessonAdapter);
    }

    @Override
    public void getDataLessonFinish(List<Lesson> lessons) {
        this.lessonsConversation = lessons;
        setupUI();
    }

    Lesson lesson;

    @Override
    public void onItemClick(int position) {
        lesson = lessonsConversation.get(position);
        if (lesson.isOpen()) {
            Intent intent = new Intent(getActivity(), VocabularyActivity.class);
            intent.putExtra(KeyData.LESSON_DATA, lesson);
            startActivityForResult(intent, VOCABULARY_ACTIVITY);
        } else {
            showUnlockLesson(position);
        }
    }


    public void showUnlockLesson(int position) {
        DialogMessage dialogMessage = new DialogMessage(getActivity());
        String contentMessage = String.format(getString(R.string.message_unlock), position);
        dialogMessage.setContentMessage(contentMessage);
        dialogMessage.showDialog();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VOCABULARY_ACTIVITY && data.hasExtra(KeyData.LESSON_DATA)) {
            Lesson lessonData = (Lesson) data.getSerializableExtra(KeyData.LESSON_DATA);
            tab2Presenter.checkUnlockLesson(lessonData);
            lesson.setCurrentStar(lessonData.getCurrentStar());
            lessonAdapter.notifyDataSetChanged();
        }
    }
}
