package com.thv.conversation.settingActivity;

import android.content.Context;

/**
 * Created by NGUYENHUONG on 12/26/16.
 */

class SettingPresenter {

    private SettingInterface mView;
    private SettingModle mModel;

    SettingPresenter(Context mContext, SettingInterface mView) {
        mModel = new SettingModle(mContext);
        this.mView = mView;
    }


    void saveTimeNotification(boolean isEnable, int time) {
        mModel.saveTimeNotification(isEnable, time);
    }

    void getTimeNotification() {
        boolean isEnable = mModel.getEnableNotification();
        int time = mModel.getTimeNotification();
        mView.finishLoadDataNoti(isEnable, time);
    }

}
