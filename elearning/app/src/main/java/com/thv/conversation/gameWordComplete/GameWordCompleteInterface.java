package com.thv.conversation.gameWordComplete;

import com.thv.conversation.util.objects.ItemWordSelect;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public interface GameWordCompleteInterface {
    void updateView(LessonDetail lessonDetail, List<ItemWordSelect> itemWorSelects);

    void updateStatusGame(int score, int canInCorrect);

    void finishGame(boolean isSuccess, int score, int highScore);

    void checkAnswerFinish(boolean isCorrect, char character);
}
