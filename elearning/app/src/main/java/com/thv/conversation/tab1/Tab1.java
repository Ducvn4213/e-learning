package com.thv.conversation.tab1;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thv.conversation.R;
import com.thv.conversation.configs.KeyData;
import com.thv.conversation.conversationActivity.ConversationActivity;
import com.thv.conversation.util.adapters.LessonAdapter;
import com.thv.conversation.util.dialog.DialogMessage;
import com.thv.conversation.util.objects.Lesson;

import java.util.List;

public class Tab1 extends Fragment implements Tab1Interfaces, LessonAdapter.OnItemClickListener {
    private static final int CONVERSATION_ACTIVITY = 1;
    private Tab1Presenter tab1Presenter;
    private RecyclerView rvLessson;
    private LessonAdapter lessonAdapter;
    private List<Lesson> lessonsConversation;
    private Lesson currentLesson;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_1, container, false);
        bindingControl(view);
        if (lessonsConversation == null) {
            getLesson();
        } else {
            setupUI();
        }
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void bindingControl(View view) {
        rvLessson = (RecyclerView) view.findViewById(R.id.recyclerview_lessson);
    }

    public void updateFromActivity() {
        if (getView() != null) {
            getLesson();
        }
    }

    public void getLesson() {
        tab1Presenter = new Tab1Presenter(getActivity(), this);
        tab1Presenter.getLessons();
    }


    public void setupUI() {
        rvLessson.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvLessson.setLayoutManager(mLayoutManager);
        lessonAdapter = new LessonAdapter(lessonsConversation, this);
        rvLessson.setAdapter(lessonAdapter);
    }

    @Override
    public void getDataLessonFinish(List<Lesson> lessons) {
        this.lessonsConversation = lessons;
        setupUI();
    }


    @Override
    public void onItemClick(int position) {
        currentLesson = lessonsConversation.get(position);
        if (currentLesson.isOpen()) {
            Intent intent = new Intent(getActivity(), ConversationActivity.class);
            intent.putExtra(KeyData.LESSON_DATA, currentLesson);
            startActivityForResult(intent, CONVERSATION_ACTIVITY);
        } else {
            showUnlockLesson(position);
        }
    }

    public void showUnlockLesson(int position) {
        DialogMessage dialogMessage = new DialogMessage(getActivity());
        String contentMessage = String.format(getString(R.string.message_unlock), position);
        dialogMessage.setContentMessage(contentMessage);
        dialogMessage.showDialog();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CONVERSATION_ACTIVITY && data.hasExtra(KeyData.LESSON_DATA)) {
                Lesson lessonData = (Lesson) data.getSerializableExtra(KeyData.LESSON_DATA);
                tab1Presenter.checkUnlockLesson(lessonData);
                currentLesson.setCurrentStar(lessonData.getCurrentStar());
                lessonAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
          //  e.printStackTrace();
        }
    }
}
