package com.thv.conversation.gameMatchWord;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.thv.conversation.MyActivity;
import com.thv.conversation.R;
import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.adapters.GameAdapter;
import com.thv.conversation.util.dialog.DialogFinishGame;
import com.thv.conversation.util.objects.ItemGame;
import com.thv.conversation.util.objects.Lesson;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/18/16.
 */

public class GameMatchWordActivity extends MyActivity implements GameMatchWordInterfaces, GameAdapter.OnItemClickListener,
        DialogFinishGame.OnClickButton {

    private GameMatchWordPresenter mPresenter;
    private ListView listView;
    private GameAdapter gameAdapter;
    private DialogFinishGame dialogFinishGame;
    private TextView tvScore, tvIncorrect;
    private ImageView ivQuizIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);
        bindingControl();
        mPresenter = new GameMatchWordPresenter(this, this);
        dialogFinishGame = new DialogFinishGame(this, this);
        if (getIntent().hasExtra(KeyData.LESSON_DATA)) {
            Lesson lesson = (Lesson) getIntent().getSerializableExtra(KeyData.LESSON_DATA);
            mPresenter.getLessonDetails(lesson);
        } else {
            mPresenter.getAllWordBookMark();
        }
    }


    private void bindingControl() {
        listView = (ListView) findViewById(R.id.rv_game);
        tvScore = (TextView) findViewById(R.id.tv_score);
        tvIncorrect = (TextView) findViewById(R.id.tv_incorrect);
        ivQuizIcon = (ImageView) findViewById(R.id.iv_quiz_icon);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getAllLessonDetail(List<ItemGame> itemGames) {
        gameAdapter = new GameAdapter(GameMatchWordActivity.this, itemGames, this);
        listView.setAdapter(gameAdapter);
    }

    @Override
    public void onItemClick(int position) {
        mPresenter.checkAnswer(position);
    }

    @Override
    public void updateRecyclerView() {
        if (gameAdapter != null) {
            gameAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClickNegative() {
        finish();
    }

    @Override
    public void onClickPositive() {
        mPresenter.resetGame();
    }

    @Override
    public void updateStatusGame(int score, int canInCorrect) {
        tvScore.setText(String.valueOf(score));
        tvIncorrect.setText(String.valueOf(canInCorrect));
        switch (canInCorrect) {
            case 0:
                ivQuizIcon.setImageResource(R.drawable.quiz_l1);
                break;
            case 1:
                ivQuizIcon.setImageResource(R.drawable.quiz_l2);
                break;
            case 2:
                ivQuizIcon.setImageResource(R.drawable.quiz_l3);
                break;
            case 3:
                ivQuizIcon.setImageResource(R.drawable.quiz_l4);
                break;
        }
    }

    @Override
    public void finishGame(boolean isSuccess, int score, int highScore) {
        dialogFinishGame.setContent(isSuccess, score, highScore);
        dialogFinishGame.showDialog();
    }

    @Override
    public void playAudio(boolean isCorrect) {
        if (isCorrect) {
            playMedia(R.raw.correct);
        } else {
            playMedia(R.raw.wrong);
        }
    }

    public void playMedia(int resource) {
        MediaPlayer playSoundNewMessage = MediaPlayer.create(GameMatchWordActivity.this, resource);
        playSoundNewMessage.start();
    }
}
