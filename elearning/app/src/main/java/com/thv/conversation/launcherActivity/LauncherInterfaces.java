package com.thv.conversation.launcherActivity;

import com.thv.conversation.util.objects.Language;

import java.util.List;

/**
 * Created by duc.nv on 11/30/2016.
 */

public class LauncherInterfaces {
    interface RequireViewOps {
        void loadLanguageFinish(List<Language> languages);

        void loadLanguageError();
    }

    interface RequirePresenterOps {
        void loadLanguageFinish(List<Language> languages);

        void loadLanguageError();
    }
}
