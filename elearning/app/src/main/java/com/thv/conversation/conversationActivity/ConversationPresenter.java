package com.thv.conversation.conversationActivity;

import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public class ConversationPresenter {

    ConversationInterfaces mView;
    ConversationModel mModle;

    public ConversationPresenter(ConversationActivity mView) {
        this.mView = mView;
        mModle = new ConversationModel(mView);
    }


    public void getLessonDetail(Lesson lesson) {
        List<LessonDetail> lessonDetails = mModle.getLessonDetail(lesson);
        mView.getLessonsDetailFinish(lessonDetails);
    }

    public void saveLessonDetail(LessonDetail lessonDetail) {
        mModle.saveLessonDetail(lessonDetail);
    }

    public int checkCorrectAnswer(LessonDetail lessonDetail, List<String> results) {
        int currentStar = lessonDetail.getStar();
        lessonDetail.checkCorrect(results);
        if (currentStar < lessonDetail.getStar()) {
            saveLessonDetail(lessonDetail);
            return lessonDetail.getStar();
        }
        return 0;
    }

}
