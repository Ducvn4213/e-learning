package com.thv.conversation.configs;

/**
 * Created by NGUYENHUONG on 1/17/17.
 */

public class TypeStatus {
    public static final int NORMAL = 0;
    public static final int LEARNING = 1;
    public static final int REVIEWING = 2;
    public static final int MASTERED = 3;
}
