package com.thv.conversation.util.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.thv.conversation.R;
import com.thv.conversation.util.objects.ItemGame;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/21/16.
 */

public class GameAdapter extends ArrayAdapter<ItemGame> {
    private Context context;
    private List<ItemGame> itemGames;
    OnItemClickListener onItemClickListener;

    public GameAdapter(Context context, List<ItemGame> itemGames, OnItemClickListener onItemClickListener) {
        super(context, R.layout.item_word_in_game);
        this.context = context;
        this.itemGames = itemGames;
        this.onItemClickListener = onItemClickListener;
    }

    private static class ViewHolder {
        TextView tvWord1, tvWord2;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_word_in_game, parent, false);
            viewHolder.tvWord1 = (TextView) convertView.findViewById(R.id.tv_word1);
            viewHolder.tvWord2 = (TextView) convertView.findViewById(R.id.tv_word2);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        ItemGame itemGame1 = itemGames.get(2 * position);
        ItemGame itemGame2 = itemGames.get(2 * position + 1);
        viewHolder.tvWord1.setText(itemGame1.getWord());
        viewHolder.tvWord2.setText(itemGame2.getWord());
        if (itemGame1.isSelected()) {
            viewHolder.tvWord1.setBackgroundResource(R.drawable.bg_item_match_word_dark);
        } else {
            viewHolder.tvWord1.setBackgroundResource(R.drawable.bg_item_match_word);
        }
        if (itemGame2.isSelected()) {
            viewHolder.tvWord2.setBackgroundResource(R.drawable.bg_item_match_word_dark);
        } else {
            viewHolder.tvWord2.setBackgroundResource(R.drawable.bg_item_match_word);
        }
        viewHolder.tvWord1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(2 * position);
            }
        });

        viewHolder.tvWord2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(2 * position + 1);
            }
        });

        return convertView;
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    @Override
    public int getCount() {
        return itemGames.size() / 2;
    }
}