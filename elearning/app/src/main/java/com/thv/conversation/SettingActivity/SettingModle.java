package com.thv.conversation.settingActivity;

import android.content.Context;
import android.content.SharedPreferences;

import com.thv.conversation.configs.Constant;
import com.thv.conversation.configs.KeyData;

/**
 * Created by NGUYENHUONG on 12/26/16.
 */

public class SettingModle {
    private SharedPreferences sharedPreferences;

    SettingModle(Context mContext) {
        sharedPreferences = mContext.getSharedPreferences(KeyData.FILE_LOCAL, Context.MODE_PRIVATE);
    }


    void saveTimeNotification(boolean isEnable, int time) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KeyData.TIME_NOTIFICATION, time);
        editor.putBoolean(KeyData.ENABLE_NOTI, isEnable);
        editor.apply();
    }

    int getTimeNotification() {
        return sharedPreferences.getInt(KeyData.TIME_NOTIFICATION, Constant.TIME_NOTIFICATION_DEFAULT);
    }

    boolean getEnableNotification() {
        return sharedPreferences.getBoolean(KeyData.ENABLE_NOTI, true);
    }
}
