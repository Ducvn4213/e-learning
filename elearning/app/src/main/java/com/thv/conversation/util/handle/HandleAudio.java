package com.thv.conversation.util.handle;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;

/**
 * Created by NGUYENHUONG on 12/7/16.
 */

public class HandleAudio {
    public static HandleAudio mInstance = null;
    private MediaPlayer mPlayer;
    private Handler handler = new Handler();
    private HandlePlayMessageInterface handlerComplete;


    private HandleAudio() {
        mPlayer = new MediaPlayer();
    }

    public static HandleAudio getIns() {
        if (mInstance == null) {
            mInstance = new HandleAudio();
        }
        return mInstance;
    }

    public void stopMediaPlayer() {
        handler.removeCallbacks(runnable);
        if (mPlayer.isPlaying()) {
            mPlayer.stop();
        }
    }

    public void playMedia(final String url, HandlePlayMessageInterface onComplete) {
        this.handlerComplete = onComplete;
        stopMediaPlayer();
        new Thread() {
            @Override
            public void run() {
                try {
                    mPlayer.reset();
                    mPlayer.setDataSource(url);
                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mPlayer.prepare();
                    mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            if (handlerComplete != null) {
                                handlerComplete.onCompletePlayAudio();
                            }
                        }
                    });
                    mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            if (handlerComplete != null) {
                                handlerComplete.onPreparedListener(mediaPlayer.getDuration());
                                handler.post(runnable);
                            }
                            mPlayer.start();
                        }
                    });
                } catch (Exception e) {
                    if (handlerComplete != null) {
                        handlerComplete.onCompletePlayAudio();
                    }
                    e.printStackTrace();
                }
            }
        }.start();
    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (handlerComplete != null) {
                handlerComplete.onUpdateSeekbar(mPlayer.getCurrentPosition());
                if (mPlayer.getCurrentPosition() < mPlayer.getDuration()) {
                    handler.postDelayed(this, 100);
                }
            }
        }
    };

    public interface HandlePlayMessageInterface {
        void onCompletePlayAudio();

        void onUpdateSeekbar(int duration);

        void onPreparedListener(int duration);
    }

}
