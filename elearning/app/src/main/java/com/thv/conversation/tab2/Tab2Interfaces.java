package com.thv.conversation.tab2;


import com.thv.conversation.util.objects.Lesson;

import java.util.List;

public interface Tab2Interfaces {
    void getDataLessonFinish(List<Lesson> lessons);
}
