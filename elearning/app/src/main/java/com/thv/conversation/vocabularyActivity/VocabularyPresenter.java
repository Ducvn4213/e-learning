package com.thv.conversation.vocabularyActivity;

import android.util.Log;

import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public class VocabularyPresenter {

    VocabularyInterfaces mView;
    VocabularyModle mModle;

    public VocabularyPresenter(VocabularyActivity mView) {
        this.mView = mView;
        mModle = new VocabularyModle(mView);
    }


    public void getLessonDetail(Lesson lesson) {
        Log.d("HUONG", "getAllLessonDetail");
        List<LessonDetail> lessonDetails = mModle.getLessonDetail(lesson);
        mView.getLessonsDetailFinish(lessonDetails);
    }

    public void saveLessonDetail(LessonDetail lessonDetail) {
        mModle.saveLessonDetail(lessonDetail);
    }

    public void bookmarkVocabulary(LessonDetail lessonDetail) {
        lessonDetail.bookMarkLesson();
        mModle.saveBookmarkVocabulary(lessonDetail);
    }

    public void removeBookmarkVocabulary(LessonDetail lessonDetail) {
        lessonDetail.removeBookMark();
        mModle.saveBookmarkVocabulary(lessonDetail);
    }
    
    public void bookMarkAllVocabulary(List<LessonDetail> listLessonDetail) {
        for (int i = 0; i < listLessonDetail.size(); i++) {
            bookmarkVocabulary((listLessonDetail.get(i)));
        }
    }

    public int checkCorrectAnswer(LessonDetail lessonDetail, List<String> results) {
        int currentStar = lessonDetail.getStar();
        lessonDetail.checkCorrect(results);
        if (currentStar < lessonDetail.getStar()) {
            saveLessonDetail(lessonDetail);
            return lessonDetail.getStar();
        }
        return 0;
    }
}
