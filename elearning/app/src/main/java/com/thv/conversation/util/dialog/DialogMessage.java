package com.thv.conversation.util.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.thv.conversation.R;

@SuppressLint("InflateParams")
public class DialogMessage implements OnClickListener {
    private Context mContext;
    private AlertDialog dialogMessage;
    private TextView tvActionPositive, tvActionNegative;
    private TextView messageTextView;
    private HandlerClickButton actionPositive, actionNegative;

    public DialogMessage(Context context) {
        this.mContext = context;
        LayoutInflater _inflater = LayoutInflater.from(mContext);
        View view = _inflater.inflate(R.layout.dialog_message, null);
        messageTextView = (TextView) view.findViewById(R.id.tv_message);
        tvActionPositive = (TextView) view.findViewById(R.id.tv_action_positive);
        tvActionNegative = (TextView) view.findViewById(R.id.tv_action_negative);
        tvActionPositive.setOnClickListener(this);
        tvActionNegative.setOnClickListener(this);
        Builder dialog = new Builder(mContext);
        dialog.setView(view);
        dialogMessage = dialog.create();
    }

    public void setCancelable(boolean flag) {
        dialogMessage.setCancelable(flag);
    }

    public void setActionPositive(HandlerClickButton handlerClickDialog) {
        this.actionPositive = handlerClickDialog;
    }

    public void setActionNegative(HandlerClickButton handlerClickDialog) {
        this.actionNegative = handlerClickDialog;
    }


    public void setContentMessage(String contentMessage) {
        messageTextView.setText(contentMessage);
    }

    public void setContentMessage(int contentMessage) {
        messageTextView.setText(contentMessage);
    }

    public void setTextActionPositive(String text) {
        tvActionPositive.setVisibility(View.VISIBLE);
        tvActionPositive.setText(text);
    }

    public void setTextActionPositive(int text) {
        tvActionPositive.setVisibility(View.VISIBLE);
        tvActionPositive.setText(text);
    }

    public void setTextActionNegative(String text) {
        tvActionNegative.setVisibility(View.VISIBLE);
        tvActionNegative.setText(text);
    }

    public void setTextActionNegative(int text) {
        tvActionNegative.setVisibility(View.VISIBLE);
        tvActionNegative.setText(text);
    }

    public void setCancelDialog(OnCancelListener cancel) {
        dialogMessage.setOnCancelListener(cancel);
    }

    public void showDialog() {
        try {
            dialogMessage.show();
        } catch (Exception e) {
        }
    }

    public void dismissDialog() {
        try {
            dialogMessage.dismiss();
        } catch (Exception e) {
        }
    }

    public void setVisibilityActionPositive(int visibility) {
        tvActionPositive.setVisibility(visibility);
    }

    public void setVisibilityActionNegative(int visibility) {
        tvActionNegative.setVisibility(visibility);
    }

    public Boolean isShowing() {
        if (dialogMessage != null) {
            if (dialogMessage.isShowing())
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        try {
            dialogMessage.dismiss();
            if (v.getId() == R.id.tv_action_positive) {
                if (actionPositive != null) {
                    actionPositive.clickButton();
                }
            } else if (v.getId() == R.id.tv_action_negative) {
                if (actionNegative != null) {
                    actionNegative.clickButton();
                }
            }
        } catch (Exception e) {
        }

    }

}
