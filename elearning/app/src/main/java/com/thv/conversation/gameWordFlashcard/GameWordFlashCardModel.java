package com.thv.conversation.gameWordFlashcard;

import android.content.Context;
import android.content.SharedPreferences;

import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.database.SQLiteManager;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public class GameWordFlashCardModel {
    SQLiteManager sqLiteManager;
    private SharedPreferences sharedPreferences;

    GameWordFlashCardModel(Context context) {
        sqLiteManager = new SQLiteManager(context);
        sharedPreferences = context.getSharedPreferences(KeyData.FILE_LOCAL, Context.MODE_PRIVATE);
    }

    List<LessonDetail> getAllWordBookMark() {
        return sqLiteManager.getAllWordBookmark();
    }

    List<LessonDetail> getLessonDetail(Lesson lesson) {
        return sqLiteManager.getLessonDetail(lesson.getId());
    }


    void updateLessonDetail(LessonDetail lessonDetail) {
        sqLiteManager.updateLessonDetail(lessonDetail);
    }

    int getHighScore() {
        return sharedPreferences.getInt(KeyData.HIGH_SCORE_GAME_WORD_FLASH_CARD, 0);
    }
}
