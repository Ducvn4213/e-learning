package com.thv.conversation.mainActivity;

import android.content.Context;
import android.util.Log;

import com.thv.conversation.configs.Constant;
import com.thv.conversation.util.api.Services;
import com.thv.conversation.util.database.SQLiteManager;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.Response;

public class MainModel implements MainInterfaces.ProvidedModelOps, Callback<List<Lesson>> {

    private MainInterfaces.RequiredPresenterOps mPresenter;
    private SQLiteManager sqLiteManager;

    public MainModel(MainInterfaces.RequiredPresenterOps mPresenter, Context context) {
        this.mPresenter = mPresenter;
        sqLiteManager = new SQLiteManager(context);
    }

    public void getDataFromServer() {
        if (sqLiteManager.checkExitsLesson(Constant.LANGUAGE_SELECT)) {
            mPresenter.loadDataFinish(true);
        } else {
            Services.getDataFromServer(this, Constant.LANGUAGE_SELECT);
        }
    }

    public void saveData(List<Lesson> lessons) {
        sqLiteManager.insertMultiLesson(lessons);
        List<LessonDetail> lessonDetails = new ArrayList<>();
        for (int i = 0; i < lessons.size(); i++) {
            lessonDetails.addAll(lessons.get(i).getLessonDetails());
        }
        sqLiteManager.insertMultiLessonDetail(lessonDetails);
        mPresenter.loadDataFinish(true);
    }

    @Override
    public void onResponse(Response<List<Lesson>> response) {
        if (response != null && response.body() != null) {
            List<Lesson> lessons = response.body();
            saveData(lessons);
        } else {
            mPresenter.loadDataFinish(false);
        }
    }

    @Override
    public void onFailure(Throwable t) {
        t.printStackTrace();
        mPresenter.loadDataFinish(false);
    }
}
