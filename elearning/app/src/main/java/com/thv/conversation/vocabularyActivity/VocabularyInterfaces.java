package com.thv.conversation.vocabularyActivity;

import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public interface VocabularyInterfaces {

    void getLessonsDetailFinish(List<LessonDetail> lessonDetails);
}
