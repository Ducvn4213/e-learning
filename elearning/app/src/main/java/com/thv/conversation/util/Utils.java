package com.thv.conversation.util;

/**
 * Created by NGUYENHUONG on 12/16/16.
 */

public class Utils {
    public static String formatDuration(int duration) {
        int m = duration / 60;
        int s = duration % 60;
        return formatDigits(m) + ":" + formatDigits(s);
    }

    public static String formatTime(int minute, int second) {
        return formatDigits(minute) + ":" + formatDigits(second);
    }

    public static String formatDigits(int num) {
        return (num < 10) ? "0" + num : Integer.toString(num);
    }
}
