package com.thv.conversation.languageActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.thv.conversation.R;
import com.thv.conversation.launcherActivity.LauncherActivity;
import com.thv.conversation.util.adapters.LanguageAdapter;
import com.thv.conversation.util.objects.Language;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/26/16.
 */

public class LanguageActivity extends AppCompatActivity implements LanguageInterface.RequireViewOps, View.OnClickListener {
    LanguagePresenter mPresenter;
    ListView lvLanguages;
    Button btnResetLanguage;
    LanguageAdapter languageAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        setupUI();
        bindingControl();
        mPresenter = new LanguagePresenter(this, this);
        mPresenter.getAllLanguages();
    }


    private void setupUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbar);

        toolbar.setTitleTextColor(getResources().getColor(R.color.textMessage));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.select_language);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void bindingControl() {
        lvLanguages = (ListView) findViewById(R.id.lv_languages);
        btnResetLanguage = (Button) findViewById(R.id.btn_reset_language);
        btnResetLanguage.setOnClickListener(this);
    }

    @Override
    public void loadDataFinish(List<Language> languages, int currentSelect) {
        languageAdapter = new LanguageAdapter(LanguageActivity.this, languages, currentSelect);
        lvLanguages.setAdapter(languageAdapter);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_reset_language) {
            int selectLanguage = languageAdapter.getCurrentLanguageSelect();
            mPresenter.resetLanguage(selectLanguage);
        }
    }

    @Override
    public void resetApplication() {
        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finishAffinity();
    }

    @Override
    public void finishActivity() {
        finish();
    }
}
