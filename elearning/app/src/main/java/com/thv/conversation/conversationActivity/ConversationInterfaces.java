package com.thv.conversation.conversationActivity;

import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public interface ConversationInterfaces {

    void getLessonsDetailFinish(List<LessonDetail> lessonDetails);
}
