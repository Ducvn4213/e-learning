package com.thv.conversation.launcherActivity;

import android.content.Context;
import android.content.SharedPreferences;

import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.api.Services;
import com.thv.conversation.util.objects.Language;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.Response;

/**
 * Created by duc.nv on 11/30/2016.
 */

public class LauncherModel {
    LauncherInterfaces.RequirePresenterOps requirePresenterOps;
    List<Language> languages;
    private SharedPreferences sharedPreferences;


    LauncherModel(Context mContext, LauncherInterfaces.RequirePresenterOps requirePresenterOps) {
        sharedPreferences = mContext.getSharedPreferences(KeyData.FILE_LOCAL, Context.MODE_PRIVATE);
        this.requirePresenterOps = requirePresenterOps;
    }


    void getAllLanguages() {
        languages = new ArrayList<>();
        Services.getLanguageFromServer(new Callback<List<Language>>() {
            @Override
            public void onResponse(Response<List<Language>> response) {
                if (response != null & response.body() != null) {
                    requirePresenterOps.loadLanguageFinish(response.body());
                } else {
                    requirePresenterOps.loadLanguageError();
                }

            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
                requirePresenterOps.loadLanguageError();
            }
        });
    }

    void saveSelectLanguage(String countryCode) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KeyData.SELECT_LANGUAGE, countryCode);
        editor.apply();
    }

    String getSelectLanguage() {
        return sharedPreferences.getString(KeyData.SELECT_LANGUAGE, "");
    }
}
