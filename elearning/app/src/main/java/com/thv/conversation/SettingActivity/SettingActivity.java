package com.thv.conversation.settingActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;

import com.thv.conversation.R;
import com.thv.conversation.util.Utils;

import java.util.Calendar;

/**
 * Created by NGUYENHUONG on 12/26/16.
 */

public class SettingActivity extends AppCompatActivity implements SettingInterface, View.OnClickListener {
    public static int REQUEST_CODE = 4652;
    private TextView tvTimeNotification;
    private SettingPresenter mPresenter;
    private Switch swNotification;
    private int timeNotification;
    private NumberPicker npSelectTime;
    private AlertDialog dialogSelectTime;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setActionbar();
        bindingControl();
        mPresenter = new SettingPresenter(this, this);
        mPresenter.getTimeNotification();
    }

    void setActionbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        toolbar.setTitle(R.string.action_settings);
        toolbar.setTitleTextColor(getResources().getColor(R.color.textMessage));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void bindingControl() {
        tvTimeNotification = (TextView) findViewById(R.id.tv_time_notification);
        swNotification = (Switch) findViewById(R.id.sw_notification);
        swNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                tvTimeNotification.setEnabled(isChecked);
                if (isChecked) {
                    disableReminder();
                } else {
                    enableReminder();
                }
                mPresenter.saveTimeNotification(isChecked, timeNotification);
            }
        });
        tvTimeNotification.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_time_notification:
                showDialogSelectTime();
                break;
            case R.id.tv_action_positive:
                if (npSelectTime != null) {
                    finishLoadDataNoti(true, npSelectTime.getValue());
                    mPresenter.saveTimeNotification(true, timeNotification);
                    dialogSelectTime.dismiss();
                }
                break;
        }

    }

    @Override
    public void finishLoadDataNoti(boolean isEnable, int time) {
        timeNotification = time;
        swNotification.setChecked(isEnable);
        tvTimeNotification.setEnabled(isEnable);
        String timeString = Utils.formatTime(timeNotification, 0);
        tvTimeNotification.setText(timeString);
    }

    public void showDialogSelectTime() {
        String[] itemTime = new String[24];
        for (int i = 0; i < 24; i++) {
            itemTime[i] = Utils.formatTime(i, 0);
        }
        LayoutInflater _inflater = LayoutInflater.from(this);
        View view = _inflater.inflate(R.layout.dialog_select_time, null);
        TextView tvActionPositive = (TextView) view.findViewById(R.id.tv_action_positive);
        npSelectTime = (NumberPicker) view.findViewById(R.id.np_select_time);
        npSelectTime.setMinValue(0);
        npSelectTime.setMaxValue(23);
        npSelectTime.setValue(timeNotification);
        npSelectTime.setDisplayedValues(itemTime);
        npSelectTime.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        tvActionPositive.setOnClickListener(this);
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setView(view);
        dialogSelectTime = dialog.create();
        dialogSelectTime.show();

    }


    private void disableReminder() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, SettingActivity.REQUEST_CODE, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(broadcast);
    }

    private void enableReminder() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, SettingActivity.REQUEST_CODE, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, timeNotification);
        calendar.set(Calendar.MINUTE, 0);
        if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
            calendar.add(Calendar.DATE, 1);
        }
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 24 * 60 * 60 * 1000, broadcast);
    }
}
