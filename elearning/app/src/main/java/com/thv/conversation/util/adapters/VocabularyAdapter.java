package com.thv.conversation.util.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thv.conversation.R;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NGUYENHUONG on 12/7/16.
 */

public class VocabularyAdapter extends RecyclerView.Adapter<VocabularyAdapter.ViewHolder> {
    private List<LessonDetail> mDataset;
    private int currentSelect = 0;
    private OnClickListener onClickListener;


    static class ViewHolder extends RecyclerView.ViewHolder {
        View layoutMessage, viewInstructor;
        TextView tvTitle, tvMean, tvMessageInstructor, tvUserAnswer;
        ImageView ivRecord, ivPlayAudio, ivBookmar;
        List<ImageView> ivStarCurrent, ivStarAnwser;

        ViewHolder(View view) {
            super(view);
            layoutMessage = view.findViewById(R.id.layout_message);
            viewInstructor = view.findViewById(R.id.view_instructor);
            ivRecord = (ImageView) view.findViewById(R.id.iv_record);
            ivPlayAudio = (ImageView) view.findViewById(R.id.iv_play_audio);
            ivBookmar = (ImageView) view.findViewById(R.id.iv_bookmar);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvMean = (TextView) view.findViewById(R.id.tv_mean);
            tvMessageInstructor = (TextView) view.findViewById(R.id.tv_message_instructor);
            tvUserAnswer = (TextView) view.findViewById(R.id.tv_user_answer);
            ivStarCurrent = bindingListStar(layoutMessage);
            ivStarAnwser = bindingListStar(viewInstructor);
        }

        private List<ImageView> bindingListStar(View viewRoot) {
            List<ImageView> imageViews = new ArrayList<>();
            imageViews.add((ImageView) viewRoot.findViewById(R.id.star_1));
            imageViews.add((ImageView) viewRoot.findViewById(R.id.star_2));
            imageViews.add((ImageView) viewRoot.findViewById(R.id.star_3));
            imageViews.add((ImageView) viewRoot.findViewById(R.id.star_4));
            imageViews.add((ImageView) viewRoot.findViewById(R.id.star_5));
            return imageViews;
        }
    }

    public VocabularyAdapter(OnClickListener onClickListener, List<LessonDetail> myDataset) {
        this.onClickListener = onClickListener;
        mDataset = myDataset;
    }

    @Override
    public VocabularyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vocabulary_detail, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        LessonDetail lessonDetail = mDataset.get(position);
        if (currentSelect == position) {
            setViewInstroduction(lessonDetail, holder);
        } else {
            holder.viewInstructor.setVisibility(View.GONE);
        }
        holder.tvMessageInstructor.setText(lessonDetail.getTitle());
        holder.tvTitle.setText(lessonDetail.getTitle());
        holder.tvMean.setText(lessonDetail.getMean());
        holder.layoutMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentSelect = position;
                VocabularyAdapter.this.notifyDataSetChanged();
            }
        });
        setImageStar(holder.ivStarCurrent, lessonDetail.getStar());
        setImageStar(holder.ivStarAnwser, lessonDetail.getStar());

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.iv_record:
                        if (onClickListener != null) onClickListener.onClickRecord(position);
                        break;
                    case R.id.iv_play_audio:
                        if (onClickListener != null) onClickListener.onClickAudioPlay(position);
                        break;
                    case R.id.iv_bookmar:
                        if (onClickListener != null) onClickListener.onClickBookmark(position);
                        break;
                }
            }
        };
        if (lessonDetail.isBookMarkWord()) {
            holder.ivBookmar.setImageResource(R.drawable.bookmar_full);
        } else {
            holder.ivBookmar.setImageResource(R.drawable.bookmar_empty);

        }
        holder.ivRecord.setOnClickListener(clickListener);
        holder.ivPlayAudio.setOnClickListener(clickListener);
        holder.ivBookmar.setOnClickListener(clickListener);

    }

    private void setViewInstroduction(LessonDetail lessonDetail, ViewHolder holder) {
        holder.viewInstructor.setVisibility(View.VISIBLE);
        setImageStar(holder.ivStarAnwser, lessonDetail.getStar());
        if (lessonDetail.getAnswers() != null) {
            holder.tvUserAnswer.setVisibility(View.VISIBLE);
            holder.tvUserAnswer.setText(lessonDetail.getAnswers());
            if (lessonDetail.getStar() > 0) {
                int color = holder.tvUserAnswer.getResources().getColor(R.color.textCorrectAnswer);
                holder.tvUserAnswer.setTextColor(color);
            } else {
                int color = holder.tvUserAnswer.getResources().getColor(R.color.textInCorrectAnswer);
                holder.tvUserAnswer.setTextColor(color);
            }
        } else {
            holder.tvUserAnswer.setVisibility(View.GONE);
        }
    }

    private void setImageStar(List<ImageView> imageStars, int star) {
        int min = (imageStars.size() > star) ? star : imageStars.size();
        for (int i = 0; i < 5; i++) {
            if (i < min) {
                imageStars.get(i).setImageResource(R.drawable.ystar_full);
            } else {
                imageStars.get(i).setImageResource(R.drawable.ystar_empty);
            }
        }
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnClickListener {
        void onClickRecord(int position);

        void onClickAudioPlay(int position);

        void onClickBookmark(int position);
    }
}

