package com.thv.conversation.vocabularyActivity;

import android.content.Context;

import com.thv.conversation.util.database.SQLiteManager;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public class VocabularyModle {
    Context mContext;
    SQLiteManager sqLiteManager;

    public VocabularyModle(Context mContext) {
        this.mContext = mContext;
        sqLiteManager = new SQLiteManager(mContext);
    }

    public List<LessonDetail> getLessonDetail(Lesson lesson) {
        return sqLiteManager.getLessonDetail(lesson.getId());
    }

    public void saveLessonDetail(LessonDetail lessonDetail) {
        sqLiteManager.updateLessonDetail(lessonDetail);
    }


    public void saveBookmarkVocabulary(LessonDetail lessonDetail) {
        sqLiteManager.saveBookmarLessonDetail(lessonDetail);
    }
}
