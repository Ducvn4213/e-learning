package com.thv.conversation.util.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.thv.conversation.R;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NGUYENHUONG on 12/7/16.
 */

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ViewHolder> {
    private List<LessonDetail> mDataset;
    int currentSelect = 0;
    OnClickListener onClickListener;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View viewOtherMessage, viewMyMessage, layoutMessage, viewInstructor;
        public TextView tvOtherMessage, tvMyMessage, tvMessageInstructor, tvUserAnswer, tvLeftMeaning, tvRightMeaning;
        public FrameLayout.LayoutParams messageParams;
        public ImageView ivRecord, ivPlayAudio, ivPlayAudioSlow, ivTest;
        public List<ImageView> listLeftDetail, listRightDetail, listStarAnwser;


        public ViewHolder(View view) {
            super(view);
            view.findViewById(R.id.layout_message);
            layoutMessage = view.findViewById(R.id.layout_message);
            viewOtherMessage = view.findViewById(R.id.view_other_message);
            viewMyMessage = view.findViewById(R.id.view_my_message);
            viewInstructor = view.findViewById(R.id.view_instructor);
            tvOtherMessage = (TextView) view.findViewById(R.id.tv_other_message);
            tvMyMessage = (TextView) view.findViewById(R.id.tv_my_message);
            tvMessageInstructor = (TextView) view.findViewById(R.id.tv_message_instructor);
            tvUserAnswer = (TextView) view.findViewById(R.id.tv_user_answer);
            tvLeftMeaning = (TextView) view.findViewById(R.id.tv_left_meaning);
            tvRightMeaning = (TextView) view.findViewById(R.id.tv_right_meaning);
            messageParams = (FrameLayout.LayoutParams) layoutMessage.getLayoutParams();
            ivRecord = (ImageView) view.findViewById(R.id.iv_record);
            ivPlayAudio = (ImageView) view.findViewById(R.id.iv_play_audio);
            ivPlayAudioSlow = (ImageView) view.findViewById(R.id.iv_play_audio_slow);
            ivTest = (ImageView) view.findViewById(R.id.iv_avatar_test);
            listLeftDetail = bindingListStar(viewOtherMessage);
            listRightDetail = bindingListStar(viewMyMessage);
            listStarAnwser = bindingListStar(viewInstructor);
        }

        private List<ImageView> bindingListStar(View viewRoot) {
            List<ImageView> imageViews = new ArrayList<>();
            imageViews.add((ImageView) viewRoot.findViewById(R.id.star_1));
            imageViews.add((ImageView) viewRoot.findViewById(R.id.star_2));
            imageViews.add((ImageView) viewRoot.findViewById(R.id.star_3));
            imageViews.add((ImageView) viewRoot.findViewById(R.id.star_4));
            imageViews.add((ImageView) viewRoot.findViewById(R.id.star_5));
            return imageViews;
        }

    }


    public ConversationAdapter(OnClickListener onClickListener, List<LessonDetail> myDataset) {
        this.onClickListener = onClickListener;
        mDataset = myDataset;
    }

    @Override
    public ConversationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lesson_detail, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        LessonDetail lessonDetail = mDataset.get(position);
        if (position % 2 == 0) {
            holder.messageParams.gravity = Gravity.LEFT;
            holder.tvOtherMessage.setText(lessonDetail.getTitle());
            holder.tvLeftMeaning.setText(lessonDetail.getMean());
            holder.viewOtherMessage.setVisibility(View.VISIBLE);
            holder.viewMyMessage.setVisibility(View.GONE);
            setImageStar(holder.listLeftDetail, lessonDetail.getStar());
        } else {
            holder.messageParams.gravity = Gravity.RIGHT;
            holder.tvMyMessage.setText(lessonDetail.getTitle());
            holder.tvRightMeaning.setText(lessonDetail.getMean());
            holder.viewOtherMessage.setVisibility(View.GONE);
            holder.viewMyMessage.setVisibility(View.VISIBLE);
            setImageStar(holder.listRightDetail, lessonDetail.getStar());
        }
        holder.layoutMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentSelect = position;
                ConversationAdapter.this.notifyDataSetChanged();

            }
        });
        if (currentSelect == position) {
            setViewInstroduction(lessonDetail, holder);
        } else {
            lessonDetail.setAnswers(null);
            holder.viewInstructor.setVisibility(View.GONE);
        }
        holder.tvMessageInstructor.setText(lessonDetail.getTitle());

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.iv_record:
                        if (onClickListener != null) onClickListener.onClickRecord(position);
                        break;
                    case R.id.iv_play_audio:
                        if (onClickListener != null) onClickListener.onClickAudioPlay(position);
                        break;
                    case R.id.iv_play_audio_slow:
                        if (onClickListener != null) onClickListener.onClickAudioPlaySlow(position);
                        break;
                    case R.id.iv_avatar_test:
                        //onClickListener.onTest(position);
                }
            }
        };
        holder.ivRecord.setOnClickListener(clickListener);
        holder.ivPlayAudio.setOnClickListener(clickListener);
        holder.ivPlayAudioSlow.setOnClickListener(clickListener);
        holder.ivTest.setOnClickListener(clickListener);
    }

    private void setViewInstroduction(LessonDetail lessonDetail, ViewHolder holder) {
        holder.viewInstructor.setVisibility(View.VISIBLE);
        setImageStar(holder.listStarAnwser, lessonDetail.getStar());
        if (lessonDetail.getAnswers() != null) {
            holder.tvUserAnswer.setVisibility(View.VISIBLE);
            holder.tvUserAnswer.setText(lessonDetail.getAnswers());
            if (lessonDetail.getStar() > 0) {
                int color = holder.tvUserAnswer.getResources().getColor(R.color.textCorrectAnswer);
                holder.tvUserAnswer.setTextColor(color);
            } else {
                int color = holder.tvUserAnswer.getResources().getColor(R.color.textInCorrectAnswer);
                holder.tvUserAnswer.setTextColor(color);
            }
        } else {
            holder.tvUserAnswer.setVisibility(View.GONE);
        }
    }


    public void setImageStar(List<ImageView> imageStars, int star) {
        int min = (imageStars.size() > star) ? star : imageStars.size();
        for (int i = 0; i < 5; i++) {
            if (i < min) {
                imageStars.get(i).setImageResource(R.drawable.ystar_full);
            } else {
                imageStars.get(i).setImageResource(R.drawable.ystar_empty);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnClickListener {
        void onClickRecord(int position);

        void onClickAudioPlay(int position);

        void onClickAudioPlaySlow(int position);
    }
}

