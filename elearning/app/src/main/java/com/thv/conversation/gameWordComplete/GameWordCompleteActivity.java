package com.thv.conversation.gameWordComplete;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.thv.conversation.MyActivity;
import com.thv.conversation.R;
import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.adapters.GameWordCompleteAdapter;
import com.thv.conversation.util.dialog.DialogFinishGame;
import com.thv.conversation.util.objects.ItemWordSelect;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public class GameWordCompleteActivity extends MyActivity implements GameWordCompleteInterface,
        GameWordCompleteAdapter.OnClickListener, DialogFinishGame.OnClickButton {
    RecyclerView rvCharactersWord;
    GameWordCompleteAdapter gameWordCompleteAdapter;
    private TextView tvScore, tvIncorrect, tvMeanWord, tvTitleWord;
    private ImageView ivQuizIcon;
    private DialogFinishGame dialogFinishGame;
    private GameWordCompletePresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_word_complete);
        bindingControl();
        setupUI();
        mPresenter = new GameWordCompletePresenter(this, this);
        dialogFinishGame = new DialogFinishGame(this, this);
        if (getIntent().hasExtra(KeyData.LESSON_DATA)) {
            Lesson lesson = (Lesson) getIntent().getSerializableExtra(KeyData.LESSON_DATA);
            mPresenter.getLessonDetails(lesson);
        } else {
            mPresenter.getAllWordBookMark();
        }
    }

    public void bindingControl() {
        rvCharactersWord = (RecyclerView) findViewById(R.id.rv_characters_word);
        tvMeanWord = (TextView) findViewById(R.id.tv_mean_word);
        tvScore = (TextView) findViewById(R.id.tv_score);
        tvIncorrect = (TextView) findViewById(R.id.tv_incorrect);
        ivQuizIcon = (ImageView) findViewById(R.id.iv_quiz_icon);
        tvTitleWord = (TextView) findViewById(R.id.tv_title_word);
    }

    public void setupUI() {
        rvCharactersWord.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 5);
        rvCharactersWord.setLayoutManager(mLayoutManager);
        gameWordCompleteAdapter = new GameWordCompleteAdapter(this);
        rvCharactersWord.setAdapter(gameWordCompleteAdapter);
    }

    @Override
    public void updateView(LessonDetail lessonDetail, List<ItemWordSelect> itemWorSelects) {
        tvMeanWord.setText(lessonDetail.getMean());
        tvTitleWord.setText("");
        gameWordCompleteAdapter.setItemWorSelects(itemWorSelects);
        gameWordCompleteAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClickItem(int position) {
        mPresenter.checkAnswer(position);
    }

    @Override
    public void updateStatusGame(int score, int canInCorrect) {
        tvScore.setText(String.valueOf(score));
        tvIncorrect.setText(String.valueOf(canInCorrect));
        switch (canInCorrect) {
            case 0:
                ivQuizIcon.setImageResource(R.drawable.quiz_l1);
                break;
            case 1:
                ivQuizIcon.setImageResource(R.drawable.quiz_l2);
                break;
            case 2:
                ivQuizIcon.setImageResource(R.drawable.quiz_l3);
                break;
            case 3:
                ivQuizIcon.setImageResource(R.drawable.quiz_l4);
                break;
        }
    }

    @Override
    public void finishGame(boolean isSuccess, int score, int highScore) {
        dialogFinishGame.setContent(isSuccess, score, highScore);
        dialogFinishGame.showDialog();
    }

    @Override
    public void checkAnswerFinish(boolean isCorrect, char character) {
        if (isCorrect) {
            tvTitleWord.setText(String.format("%s%c", tvTitleWord.getText().toString(), character));
        }
        gameWordCompleteAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClickNegative() {
        finish();
    }

    @Override
    public void onClickPositive() {
        mPresenter.resetGame();
    }
}
