package com.thv.conversation.gameWordFlashcard;

import android.content.Context;

import com.thv.conversation.configs.TypeStatus;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.Collections;
import java.util.List;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public class GameWordFlashCardPresenter {
    private GameWordFlashCardModel mModel;
    private GameWordFlashCardInterface mView;
    private List<LessonDetail> lessonDetails;
    private int currentSelect = -1, countWordMaster = 0, countWordMReviewing = 0, countWordMLearning = 0;

    GameWordFlashCardPresenter(Context context, GameWordFlashCardInterface mView) {
        this.mModel = new GameWordFlashCardModel(context);
        this.mView = mView;
    }

    void getAllWordBookMark() {
        lessonDetails = mModel.getAllWordBookMark();
        updateData();
        nextWord();
    }


    void getLessonDetails(Lesson lesson) {
        lessonDetails = mModel.getLessonDetail(lesson);
        updateData();
        nextWord();
    }

    private void updateData() {
        for (int i = lessonDetails.size() - 1; i >= 0; i--) {
            if (lessonDetails.get(i).getTitle().matches("[a-zA-Z ]*\\d+.*")) {
                lessonDetails.remove(i);
            }
        }
        Collections.shuffle(lessonDetails);
        calStatusLessonDetail();
    }


    private void calStatusLessonDetail() {
        for (int i = 0; i < lessonDetails.size(); i++) {
            switch (lessonDetails.get(i).getStatus()) {
                case TypeStatus.LEARNING:
                    countWordMLearning++;
                    break;
                case TypeStatus.REVIEWING:
                    countWordMReviewing++;
                    break;
                case TypeStatus.MASTERED:
                    countWordMaster++;
                    break;
            }
        }
    }

    void answerWord(boolean isKnow) {
        LessonDetail currentLesson = lessonDetails.get(currentSelect);
        int currentStatus = currentLesson.getStatus();
        if (isKnow) {
            switch (currentStatus) {
                case TypeStatus.NORMAL:
                    currentLesson.setStatus(TypeStatus.MASTERED);
                    countWordMaster++;
                    break;
                case TypeStatus.REVIEWING:
                    currentLesson.setStatus(TypeStatus.MASTERED);
                    countWordMaster++;
                    countWordMReviewing--;
                    break;
                case TypeStatus.LEARNING:
                    currentLesson.setStatus(TypeStatus.REVIEWING);
                    countWordMLearning--;
                    countWordMReviewing++;
            }
        } else {
            switch (currentStatus) {
                case TypeStatus.NORMAL:
                    currentLesson.setStatus(TypeStatus.LEARNING);
                    countWordMLearning++;
                    break;
                case TypeStatus.REVIEWING:
                    currentLesson.setStatus(TypeStatus.LEARNING);
                    countWordMReviewing--;
                    countWordMLearning++;

                case TypeStatus.MASTERED:
                    currentLesson.setStatus(TypeStatus.REVIEWING);
                    countWordMaster--;
                    countWordMReviewing++;
                    break;

            }
        }
        mModel.updateLessonDetail(currentLesson);
        nextWord();
    }

    private void nextWord() {
        currentSelect++;
        if (currentSelect >= lessonDetails.size()) {
            currentSelect = 0;
            Collections.shuffle(lessonDetails);
        }
        mView.updateView(lessonDetails.get(currentSelect));
        mView.updateStatusGame(countWordMaster, countWordMReviewing, countWordMLearning, lessonDetails.size());
    }
}
