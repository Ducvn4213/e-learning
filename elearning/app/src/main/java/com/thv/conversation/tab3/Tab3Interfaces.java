package com.thv.conversation.tab3;


import com.thv.conversation.util.objects.Lesson;

import java.util.List;

public interface Tab3Interfaces {
    void getDataLessonFinish(List<Lesson> lessons);
}
