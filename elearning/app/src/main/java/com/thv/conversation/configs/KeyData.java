package com.thv.conversation.configs;

/**
 * Created by NGUYENHUONG on 12/6/16.
 */

public class KeyData {
    public static final String FILE_LOCAL = "ENGLISH_SPEAKING";
    public static final String SELECT_LANGUAGE = "SELECT_LANGUAGE";
    public static final String HIGH_SCORE = "HIGH_SCORE";
    public static final String HIGH_SCORE_GAME_WORD_SELECT = "GAME_WORD_SELECT";
    public static final String HIGH_SCORE_GAME_WORD_COMPLETE = "GAME_WORD_COMPLETE";
    public static final String HIGH_SCORE_GAME_WORD_FLASH_CARD = "GAME_WORD_FLASH_CARD";
    public static final String LESSON_DATA = "LESSON_DATA";
    public static final String TIME_NOTIFICATION = "TIME_NOTIFICATION";
    public static final String ENABLE_NOTI = "ENABLE_NOTI";


}
