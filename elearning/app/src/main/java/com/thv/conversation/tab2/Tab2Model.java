package com.thv.conversation.tab2;

import android.content.Context;

import com.thv.conversation.configs.Constant;
import com.thv.conversation.configs.TypeLesson;
import com.thv.conversation.util.database.SQLiteManager;
import com.thv.conversation.util.objects.Lesson;

import java.util.List;

public class Tab2Model {
    Context mContext;
    SQLiteManager sqLiteManager;

    public Tab2Model(Context mContext) {
        this.mContext = mContext;
        sqLiteManager = new SQLiteManager(mContext);
    }

    public List<Lesson> loadDataCourse() {
        List<Lesson> lessons = sqLiteManager.getAllLesson(Constant.LANGUAGE_SELECT, TypeLesson.VOCABULARY);
        return lessons;
    }

    public void unLockLesson(Lesson lesson) {
        sqLiteManager.unLockLesson(lesson);
    }


}
