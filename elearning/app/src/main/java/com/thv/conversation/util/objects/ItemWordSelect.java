package com.thv.conversation.util.objects;

/**
 * Created by NGUYENHUONG on 1/16/17.
 */

public class ItemWordSelect {
    private boolean isSelect;
    private char _char;

    public ItemWordSelect(char _char) {
        this.isSelect = false;
        this._char = _char;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public char getChar() {
        return _char;
    }

    public void set_char(char _char) {
        this._char = _char;
    }
}
