package com.thv.conversation.util.objects;

/**
 * Created by NGUYENHUONG on 12/20/16.
 */

public class ItemGame {
    private String word;
    private String answer;
    private boolean isSelected;

    public ItemGame(String word, String answer) {
        this.word = word;
        this.answer = answer;
        isSelected = false;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean checkAnswer(String answer) {
        if (this.answer.equals(answer)) return true;
        return false;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
