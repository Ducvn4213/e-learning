package com.thv.conversation.tab3;

import android.content.Context;

import com.thv.conversation.configs.Constant;
import com.thv.conversation.configs.TypeLesson;
import com.thv.conversation.util.database.SQLiteManager;
import com.thv.conversation.util.objects.Lesson;

import java.util.List;

public class Tab3Model {
    private Context mContext;
    private SQLiteManager sqLiteManager;

    public Tab3Model(Context mContext) {
        this.mContext = mContext;
        sqLiteManager = new SQLiteManager(mContext);
    }

    public List<Lesson> loadDataCourse() {
        return sqLiteManager.getAllLesson(Constant.LANGUAGE_SELECT, TypeLesson.PHRASE);
    }

    public void unLockLesson(Lesson lesson) {
        sqLiteManager.unLockLesson(lesson);
    }
}
