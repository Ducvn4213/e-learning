package com.thv.conversation.gameWordFlashcard;

import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thv.conversation.R;
import com.thv.conversation.configs.TypeStatus;
import com.thv.conversation.util.objects.LessonDetail;

/**
 * Created by NGUYENHUONG on 1/16/17.
 */

public class CardFrontFragment extends Fragment {
    TextView tvWord, tvStatus;
    LessonDetail lessonDetail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_front_flash_card, container, false);
        bindingControl(view);
        if (lessonDetail != null) updateView();
        return view;

    }

    private void bindingControl(View view) {
        tvWord = (TextView) view.findViewById(R.id.tv_word);
        tvStatus = (TextView) view.findViewById(R.id.tv_status);
    }

    public void updateView() {

        if (tvWord != null && isAdded()) {
            tvWord.setText(lessonDetail.getTitle());
            switch (lessonDetail.getStatus()) {
                case TypeStatus.LEARNING:
                    tvStatus.setText(R.string.learning);
                    tvStatus.setTextColor(getResources().getColor(R.color.colorViewLearningWord));
                    break;
                case TypeStatus.REVIEWING:
                    tvStatus.setText(R.string.reviewing);
                    tvStatus.setTextColor(getResources().getColor(R.color.colorViewReviewingWord));
                    break;
                case TypeStatus.MASTERED:
                    tvStatus.setText(R.string.mastered);
                    tvStatus.setTextColor(getResources().getColor(R.color.colorViewMasteredWord));
                    break;
            }
        }
    }

    public void updateView(LessonDetail lessonDetail) {
        this.lessonDetail = lessonDetail;
        updateView();
    }
}
