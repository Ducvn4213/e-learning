package com.thv.conversation.gameMatchWord;

import com.thv.conversation.util.objects.ItemGame;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/18/16.
 */

public interface GameMatchWordInterfaces {
    void getAllLessonDetail(List<ItemGame> itemGames);

    void updateRecyclerView();

    void updateStatusGame(int score, int canInCorrect);

    void finishGame(boolean isSuccess, int score, int highScore);

    void playAudio(boolean isCorrect);
}
