package com.thv.conversation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

/**
 * Created by NGUYENHUONG on 12/26/16.
 */

public class MyActivity extends AppCompatActivity {
    protected LinearLayout layoutBannerAds;
    protected AdView adView;
    protected com.facebook.ads.AdView adFView;
    protected InterstitialAd interstitialAd;
    protected com.facebook.ads.InterstitialAd interstitialAdFB;
    boolean isShowAds = true;


    @Override
    protected void onStart() {
        super.onStart();
        if (!isShowAds) {
           // loadInterstitialAdsGoogle();
            loadInterstitialAdsFacebook();
            isShowAds = true;
            layoutBannerAds = (LinearLayout) findViewById(R.id.layout_banner_ads);
            if (layoutBannerAds != null) {
             //   addBannerAdmob();
                addBannerFacebook();
            }
        }
    }

    @Override
    public void finish() {
        if (adFView != null) adFView.destroy();
        super.finish();
    }

    private void addBannerAdmob() {
        String keyBannerGoogle = getString(R.string.key_banner_ads_google);
        if (layoutBannerAds == null) return;
        adView = new AdView(this);
        adView.setAdUnitId(keyBannerGoogle);
        adView.setAdSize(AdSize.BANNER);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                layoutBannerAds.removeAllViews();
                layoutBannerAds.addView(adView);
                layoutBannerAds.setVisibility(View.VISIBLE);
            }
        });
    }


    private void addBannerFacebook() {
        String keyFacebook = getString(R.string.key_banner_ads_facebook);
        if (layoutBannerAds == null) return;
        //AdSettings.addTestDevice("9c2747c6a440e8ce79e2b417f712ed7c");
        adFView = new com.facebook.ads.AdView(this, keyFacebook, com.facebook.ads.AdSize.BANNER_HEIGHT_50);
        adFView.setAdListener(new AbstractAdListener() {
            @Override
            public void onAdLoaded(Ad ad) {
                if (layoutBannerAds != null) {
                    layoutBannerAds.removeAllViews();
                    layoutBannerAds.addView(adFView);
                    layoutBannerAds.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(Ad ad, AdError adError) {

            }
        });
        adFView.loadAd();
    }

    public void loadInterstitialAdsGoogle() {
        String keyFullScreenGoogle = getString(R.string.key_fullscreen_ads_google);
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(keyFullScreenGoogle);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                AdRequest adRequest = new AdRequest.Builder().build();
                interstitialAd.loadAd(adRequest);
            }
        });
        AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd.loadAd(adRequest);

    }


    protected void loadInterstitialAdsFacebook() {
        String keyFacebook = getString(R.string.key_fullscreen_ads_facebook);
        interstitialAdFB = new com.facebook.ads.InterstitialAd(this, keyFacebook);
        interstitialAdFB.setAdListener(new AbstractAdListener() {
            @Override
            public void onInterstitialDismissed(Ad ad) {
                super.onInterstitialDismissed(ad);
                interstitialAdFB.loadAd();
            }
        });
        interstitialAdFB.loadAd();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (interstitialAdFB != null && interstitialAdFB.isAdLoaded()) {
            interstitialAdFB.show();
        } else if (interstitialAd != null && interstitialAd.isLoaded()) {
            interstitialAd.show();
        }
    }

}