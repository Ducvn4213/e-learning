package com.thv.conversation.mainActivity;


public class MainPresenter implements MainInterfaces.ProvidedPresenterOps, MainInterfaces.RequiredPresenterOps {

    MainInterfaces.RequiredViewOps mView;
    MainModel mModel;

    public MainPresenter(MainActivity mView) {
        this.mView = mView;
        mModel = new MainModel(this, mView);
    }

    public void getDataFromServer() {
        mModel.getDataFromServer();
    }

    @Override
    public void loadDataFinish(boolean isSuccess) {
        mView.loadDataFinish(isSuccess);
    }


}
