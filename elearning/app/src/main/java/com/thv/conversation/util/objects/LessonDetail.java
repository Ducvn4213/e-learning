package com.thv.conversation.util.objects;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/7/16.
 */

public class LessonDetail {
    @SerializedName("")
    private int id;
    @SerializedName("lesson_id")
    private int idLesson;
    @SerializedName("title")
    private String title;
    @SerializedName("meaning")
    private String mean;
    @SerializedName("audio")
    private String audio;
    @SerializedName("audio_2")
    private String audioSlow;

    private int star;
    private int status;
    private int timeSave;
    private String answers;

    public LessonDetail(int id, int idLesson, String title, String mean, String audio, String audioSlow, int star, int timeSave, int status) {
        this.id = id;
        this.idLesson = idLesson;
        this.title = title;
        this.mean = mean;
        this.audio = audio;
        this.audioSlow = audioSlow;
        this.star = star;
        this.timeSave = timeSave;
        this.status = status;
    }

    public LessonDetail(String title, String mean) {
        this.idLesson = 0;
        this.title = title;
        this.mean = mean;
        this.audio = "";
        this.audioSlow = "";
        this.star = 0;
        bookMarkLesson();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdLesson() {
        return idLesson;
    }

    public void setIdLesson(int idLesson) {
        this.idLesson = idLesson;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMean() {
        return mean;
    }

    public void setMean(String mean) {
        this.mean = mean;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getAudioSlow() {
        return audioSlow;
    }

    public void setAudioSlow(String audioSlow) {
        this.audioSlow = audioSlow;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public int getTimeSave() {
        return timeSave;
    }

    public void setTimeSave(int timeSave) {
        this.timeSave = timeSave;
    }

    public boolean isBookMarkWord() {
        if (timeSave > 0) {
            return true;
        }
        return false;
    }

    public void bookMarkLesson() {
        this.timeSave = (int) (System.currentTimeMillis() / 1000);
    }

    public void removeBookMark() {
        this.timeSave = 0;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void checkCorrect(List<String> results) {
        double max = 0;
        for (int i = 0; i < results.size(); i++) {
            double similar = similarity(title, results.get(i));
            if (similar > max) {
                max = similar;
                answers = results.get(i);
            }
            if (similar == 1) {
                star = 5;
                break;
            }
        }
    }

    private double similarity(String s1, String s2) {
        String longer = s1, shorter = s2;
        if (s1.length() < s2.length()) {
            longer = s2;
            shorter = s1;
        }
        int longerLength = longer.length();
        if (longerLength == 0) {
            return 1.0;
        }
        return (longerLength - editDistance(longer, shorter)) / (double) longerLength;

    }

    private int editDistance(String s1, String s2) {
        s1 = s1.replaceAll("[.,?!]", "");
        s2 = s2.replaceAll("[.,?!]", "");
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        int[] costs = new int[s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++) {
            int lastValue = i;
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0)
                    costs[j] = j;
                else {
                    if (j > 0) {
                        int newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1))
                            newValue = Math.min(Math.min(newValue, lastValue),
                                    costs[j]) + 1;
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0)
                costs[s2.length()] = lastValue;
        }
        return costs[s2.length()];
    }

}
