package com.thv.conversation.tab1;

import android.content.Context;

import com.thv.conversation.util.objects.Lesson;

import java.util.List;

public class Tab1Presenter {
    private Tab1Interfaces mView;
    private Tab1Model mModel;
    List<Lesson> lessons;

    Tab1Presenter(Context context, Tab1Interfaces mView) {
        this.mView = mView;
        mModel = new Tab1Model(context);
    }

    void getLessons() {
        lessons = mModel.loadDataCourse();
        mView.getDataLessonFinish(lessons);
    }

    void checkUnlockLesson(Lesson lesson) {
        boolean ischeckPass = false;
        if (lessons != null && lesson.isPassLesson()) {
            for (int i = 0; i < lessons.size(); i++) {
                if (lessons.get(i).getId() == lesson.getId()) {
                    if (!lessons.get(i).isPassLesson()) {
                        ischeckPass = true;
                    } else {
                        break;
                    }
                } else {
                    if (ischeckPass) {
                        if (!lessons.get(i).isOpen()) {
                            lessons.get(i).setUnlock(1);
                            mModel.unLockLesson(lesson);
                            break;
                        }
                    }
                }

            }
        }
    }

}
