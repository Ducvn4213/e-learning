package com.thv.conversation.tab1;


import com.thv.conversation.util.objects.Lesson;

import java.util.List;

public interface Tab1Interfaces {
    void getDataLessonFinish(List<Lesson> lessons);
}
