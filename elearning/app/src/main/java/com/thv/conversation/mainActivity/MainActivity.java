package com.thv.conversation.mainActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.thv.conversation.MyActivity;
import com.thv.conversation.R;
import com.thv.conversation.configs.Constant;
import com.thv.conversation.languageActivity.LanguageActivity;
import com.thv.conversation.settingActivity.SettingActivity;
import com.thv.conversation.tab3.Tab3;
import com.thv.conversation.yourWordActivity.YourWordActivity;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import com.thv.conversation.tab1.Tab1;
import com.thv.conversation.tab2.Tab2;

public class MainActivity extends MyActivity
        implements MainInterfaces.RequiredViewOps, SmartTabLayout.TabProvider, ViewPager.OnPageChangeListener {

    private FragmentPagerItemAdapter mPagerAdapter;
    private ViewPager mViewPager;
    private TextView mTitle;
    private DrawerLayout drawer;
    private MainPresenter presenter;
    private ProgressDialog progress;
    private int currentTabSelect = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindingControl();
        setupUI();
        setupTab();
        showDialogLoading();
        presenter = new MainPresenter(this);
        presenter.getDataFromServer();
    }

    public void showDialogLoading() {
        progress = ProgressDialog.show(this, null, getString(R.string.load_data), true);
        progress.show();
    }

    private void bindingControl() {
        mViewPager = (ViewPager) findViewById(R.id.vp_container);
        mTitle = (TextView) findViewById(R.id.tv_title);
    }

    public void onClickItemDrawerLayout(View view) {
        switch (view.getId()) {
            case R.id.view_change_language:
                actionClickChangeLanguage();
                break;
            case R.id.view_rating:
                actionClickRating();
                break;
            case R.id.view_other_app:
                actionClickOtherApp();
                break;
            case R.id.view_share:
                actionClickShare();
                break;
            case R.id.view_fanpage:
                actionClickFacebook();
                break;
            case R.id.view_group:
                actionClickGroupFacebook();
                break;
            case R.id.view_setting:
                actionClickSetting();
                break;
            case R.id.view_about:
                actionClickAbout();
                break;
        }
        drawer.closeDrawers();
    }

    private void actionClickFacebook() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.URL_FACEBOOK));
        startActivity(browserIntent);
    }

    private void actionClickGroupFacebook() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.URL_GROUP_FACEBOOK));
        startActivity(browserIntent);
    }

    private void actionClickRating() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.URL_REVIEW));
        startActivity(browserIntent);
    }

    private void actionClickOtherApp() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.URL_OTHER_APP));
        startActivity(browserIntent);
    }

    private void actionClickShare() {
        final String appPackageName = getPackageName();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.message_share) + appPackageName);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void actionClickChangeLanguage() {
        Intent intent = new Intent(MainActivity.this, LanguageActivity.class);
        startActivity(intent);
    }

    private void actionClickSetting() {
        Intent intent = new Intent(MainActivity.this, SettingActivity.class);
        startActivity(intent);
    }

    private void actionClickAbout() {
        LayoutInflater _inflater = LayoutInflater.from(MainActivity.this);
        View view = _inflater.inflate(R.layout.dialog_about, null);
        TextView tvVersion = (TextView) view.findViewById(R.id.tv_version);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            tvVersion.setText(String.format(getString(R.string.version_format), version));
        } catch (PackageManager.NameNotFoundException e) {
            tvVersion.setVisibility(View.GONE);
            e.printStackTrace();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(view);
        builder.create().show();
    }


    private void setupUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(null);
        }
        drawer = (DrawerLayout) findViewById(R.id.dl_drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (currentTabSelect == 1) {
            getMenuInflater().inflate(R.menu.main, menu);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_words:
                startActivityYourWord();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setupTab() {
        mPagerAdapter = new FragmentPagerItemAdapter(getSupportFragmentManager(),
                FragmentPagerItems.with(this)
                        .add(R.string.tab1_title, Tab1.class)
                        .add(R.string.tab2_title, Tab2.class)
                        .add(R.string.tab3_title, Tab3.class)
                        .create());
        mViewPager.setAdapter(mPagerAdapter);
        SmartTabLayout smartTabLayout = (SmartTabLayout) findViewById(R.id.stl_tab);
        smartTabLayout.setCustomTabView(this);
        smartTabLayout.setViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(this);
        changeTitle(mPagerAdapter.getPageTitle(0));
    }

    private void changeTitle(final CharSequence title) {
        mTitle.setText(title);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        this.currentTabSelect = position;
        changeTitle(mPagerAdapter.getPageTitle(position));
        invalidateOptionsMenu();

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View tab = inflater.inflate(R.layout.layout_tab_main, container, false);

        ImageView icon = (ImageView) tab.findViewById(R.id.iv_icon);
        switch (position) {
            case 0:
                icon.setImageResource(R.drawable.tab_send_gig);
                break;
            case 1:
                icon.setImageResource(R.drawable.tab_manage);
                break;
            case 2:
                icon.setImageResource(R.drawable.tab_people);
                break;
            default:
                break;
        }
        TextView title = (TextView) tab.findViewById(R.id.tv_title);
        title.setText(adapter.getPageTitle(position));
        return tab;
    }


    @Override
    public void loadDataFinish(boolean isSuccess) {
        progress.dismiss();
        if (isSuccess) {
            if (mPagerAdapter.getPage(0) != null)
                ((Tab1) mPagerAdapter.getPage(0)).updateFromActivity();
            if (mPagerAdapter.getPage(1) != null)
                ((Tab2) mPagerAdapter.getPage(1)).updateFromActivity();
            if (mPagerAdapter.getPage(2) != null)
                ((Tab3) mPagerAdapter.getPage(2)).updateFromActivity();
        } else {
            showDialogError();
        }
    }

    public void showDialogError() {
        Toast.makeText(MainActivity.this, R.string.error_connect_server, Toast.LENGTH_LONG).show();
    }

    public void startActivityYourWord() {
        Intent intent = new Intent(MainActivity.this, YourWordActivity.class);
        startActivity(intent);
    }
}
