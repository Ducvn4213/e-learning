package com.thv.conversation.util.objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by NGUYENHUONG on 12/7/16.
 */

public class Lesson implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private int type;
    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("audio")
    private String audio;
    @SerializedName("language")
    private String idLanguage;
    @SerializedName("details")
    private List<LessonDetail> lessonDetails;

    private int unlock;
    private int totalStar;
    private int currentStar;


    public Lesson(int id, int type, String title, String audio, String image, int unlock, String idLanguage) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.image = image;
        this.audio = audio;
        this.unlock = unlock;
        this.idLanguage = idLanguage;
    }

    public void increaseCurrentStar(int star) {
        currentStar += star;
    }

    public int getCurrentStar() {
        return currentStar;
    }

    public void setCurrentStar(int currentStar) {
        this.currentStar = currentStar;
    }

    public int getTotalStar() {
        return totalStar;
    }

    public void setTotalStar(int totalStar) {
        this.totalStar = totalStar;
    }

    public List<LessonDetail> getLessonDetails() {
        return lessonDetails;
    }

    public void setLessonDetails(List<LessonDetail> lessonDetails) {
        this.lessonDetails = lessonDetails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        if (title == null) return "";
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        if (image == null) return "";
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAudio() {
        if (audio == null) return "";
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public int getNeedPass() {
        return totalStar * 7 / 10;
    }

    public boolean isPassLesson() {
        if (currentStar >= getNeedPass()) {
            return true;
        }
        return false;
    }

    public String getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(String idLanguage) {
        this.idLanguage = idLanguage;
    }

    public int getUnlock() {
        return unlock;
    }

    public void setUnlock(int unlock) {
        this.unlock = unlock;
    }

    public boolean isOpen() {
        return unlock == 1;
    }
}
