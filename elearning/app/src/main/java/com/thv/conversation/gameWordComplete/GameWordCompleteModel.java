package com.thv.conversation.gameWordComplete;

import android.content.Context;
import android.content.SharedPreferences;

import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.database.SQLiteManager;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public class GameWordCompleteModel {
    SQLiteManager sqLiteManager;
    private SharedPreferences sharedPreferences;

    GameWordCompleteModel(Context context) {
        sqLiteManager = new SQLiteManager(context);
        sharedPreferences = context.getSharedPreferences(KeyData.FILE_LOCAL, Context.MODE_PRIVATE);
    }

    List<LessonDetail> getAllWordBookMark() {
        return sqLiteManager.getAllWordBookmarkForGame();
    }

    List<LessonDetail> getLessonDetail(Lesson lesson) {
        return sqLiteManager.getLessonDetail(lesson.getId());
    }

    int getHighScore() {
        return sharedPreferences.getInt(KeyData.HIGH_SCORE_GAME_WORD_COMPLETE, 0);
    }

    void saveHighScore(int highScore) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KeyData.HIGH_SCORE_GAME_WORD_COMPLETE, highScore);
        editor.commit();
    }
}
