package com.thv.conversation.conversationActivity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.thv.conversation.MyActivity;
import com.thv.conversation.R;
import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.Utils;
import com.thv.conversation.util.adapters.ConversationAdapter;
import com.thv.conversation.util.dialog.DialogResult;
import com.thv.conversation.util.handle.HandleAudio;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public class ConversationActivity extends MyActivity implements ConversationInterfaces,
        ConversationAdapter.OnClickListener, HandleAudio.HandlePlayMessageInterface, View.OnClickListener {
    private static final int RESULT_SPEECH = 102;
    private static final int MAX_RESULT = 3;

    private RecyclerView rvLessonDetail;
    private TextView tvDurationMedia, tvCurrentProgress;
    private ImageView ivPlayPauseMedia;
    private SeekBar sbProgressMedia;
    private List<LessonDetail> listLessonDetail;
    private ConversationAdapter lessonDetailAdapter;
    private Lesson lesson;
    private ConversationPresenter mPresenter;
    private LessonDetail lessonDetailCurent;
    private DialogResult dialogResult;
    private boolean isPlayingAudio = false, isLoading = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_detail);
        lesson = (Lesson) getIntent().getSerializableExtra(KeyData.LESSON_DATA);
        setActionbar();
        bindingControl();
        mPresenter = new ConversationPresenter(this);
        mPresenter.getLessonDetail(lesson);
        dialogResult = new DialogResult(ConversationActivity.this);
    }

    void setActionbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        toolbar.setTitle(lesson.getTitle());
        toolbar.setTitleTextColor(getResources().getColor(R.color.textMessage));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chart, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.view_chart:
                showDialogResult();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_play_pause_media) {
            if (isPlayingAudio) {
                HandleAudio.getIns().stopMediaPlayer();
                ivPlayPauseMedia.setImageResource(R.drawable.ic_play);
                isPlayingAudio = false;
                isLoading = false;
            } else {
                if (!isLoading) {
                    isLoading = true;
                    tvDurationMedia.setText("loading");
                    ivPlayPauseMedia.setImageResource(R.drawable.ic_pause);
                    HandleAudio.getIns().playMedia(lesson.getAudio(), ConversationActivity.this);
                    isPlayingAudio = true;
                }
            }
        }
    }

    private void bindingControl() {
        rvLessonDetail = (RecyclerView) findViewById(R.id.recyclerview_lessson_detail);
        tvDurationMedia = (TextView) findViewById(R.id.tv_duration_media);
        tvCurrentProgress = (TextView) findViewById(R.id.tv_current_progress);
        ivPlayPauseMedia = (ImageView) findViewById(R.id.iv_play_pause_media);
        sbProgressMedia = (SeekBar) findViewById(R.id.sb_progress_media);
        ivPlayPauseMedia.setOnClickListener(this);
        sbProgressMedia.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    public void setupUI() {
        rvLessonDetail.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvLessonDetail.setLayoutManager(mLayoutManager);
        lessonDetailAdapter = new ConversationAdapter(this, listLessonDetail);
        rvLessonDetail.setAdapter(lessonDetailAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {
                    List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    int star = mPresenter.checkCorrectAnswer(lessonDetailCurent, results);
                    lesson.increaseCurrentStar(star);
                    lessonDetailAdapter.notifyDataSetChanged();
                }
                break;
            }

        }
    }

    @Override
    public void finish() {
        HandleAudio.getIns().stopMediaPlayer();
        getIntent().putExtra(KeyData.LESSON_DATA, lesson);
        setResult(Activity.RESULT_OK, getIntent());
        super.finish();
    }


    @Override
    public void onClickRecord(int position) {
        String language = "en-US";
        lessonDetailCurent = listLessonDetail.get(position);
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, language);
        intent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, language);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, language);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language);
        intent.putExtra(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES, language);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, lessonDetailCurent.getTitle());
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, MAX_RESULT);
        try {
            startActivityForResult(intent, RESULT_SPEECH);
        } catch (ActivityNotFoundException a) {
            Toast t = Toast.makeText(getApplicationContext(), R.string.devices_dont_support, Toast.LENGTH_SHORT);
            t.show();
        }

    }

    @Override
    public void getLessonsDetailFinish(List<LessonDetail> lessonDetails) {
        listLessonDetail = lessonDetails;
        setupUI();
    }

    @Override
    public void onClickAudioPlay(final int position) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LessonDetail lessonDetail = listLessonDetail.get(position);
                HandleAudio.getIns().playMedia(lessonDetail.getAudio(), null);
            }
        });
    }

    @Override
    public void onClickAudioPlaySlow(final int position) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LessonDetail lessonDetail = listLessonDetail.get(position);
                HandleAudio.getIns().playMedia(lessonDetail.getAudioSlow(), null);
            }
        });

    }

    @Override
    public void onCompletePlayAudio() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ivPlayPauseMedia.setImageResource(R.drawable.ic_play);
                isPlayingAudio = false;
            }
        });
    }

    @Override
    public void onPreparedListener(final int duration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                isLoading = false;
                ivPlayPauseMedia.setImageResource(R.drawable.ic_pause);
                tvDurationMedia.setText(Utils.formatDuration(duration / 1000));
                sbProgressMedia.setMax(duration);
            }
        });
    }

    @Override
    public void onUpdateSeekbar(final int duration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvCurrentProgress.setText(Utils.formatDuration(duration / 1000));
                sbProgressMedia.setProgress(duration);
            }
        });
    }


    public void showDialogResult() {
        int averageScore = 0;
        int mCompleted = 0;
        int mInCompleted = 0;
        if (listLessonDetail.size() > 0) {
            for (int i = 0; i < listLessonDetail.size(); i++) {
                averageScore += listLessonDetail.get(i).getStar();
                if (listLessonDetail.get(i).getStar() > 0) {
                    mCompleted++;
                } else {
                    mInCompleted++;
                }
            }
            averageScore = averageScore / listLessonDetail.size();
        }
        dialogResult.setResult(averageScore, mCompleted, mInCompleted);
        dialogResult.showDialog();
    }
}
