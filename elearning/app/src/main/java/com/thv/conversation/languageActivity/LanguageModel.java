package com.thv.conversation.languageActivity;

import android.content.Context;
import android.content.SharedPreferences;

import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.api.Services;
import com.thv.conversation.util.objects.Language;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.Response;

/**
 * Created by NGUYENHUONG on 12/26/16.
 */

public class LanguageModel {
    private Context mContext;
    private LanguageInterface.RequirePresenterOps mPresenterOps;
    private SharedPreferences sharedPreferences;
    List<Language> languages;

    LanguageModel(Context context, LanguageInterface.RequirePresenterOps mPresenterOps) {
        this.mContext = context;
        this.mPresenterOps = mPresenterOps;
        sharedPreferences = mContext.getSharedPreferences(KeyData.FILE_LOCAL, mContext.MODE_PRIVATE);

    }

    void getAllLanguages() {
        final String currentCountryCode = sharedPreferences.getString(KeyData.SELECT_LANGUAGE, "");
        Services.getLanguageFromServer(new Callback<List<Language>>() {
            @Override
            public void onResponse(Response<List<Language>> response) {
                if (response != null && response.body() != null) {
                    languages = response.body();
                    int currentSelect = -1;
                    for (int i = 0; i < languages.size(); i++) {
                        if (languages.get(i).getValue().equals(currentCountryCode)) {
                            currentSelect = i;
                        }
                    }
                    mPresenterOps.loadDataFinish(languages, currentSelect);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
    }


    void saveLanguageSelect(int selectLanguage) {
        if (selectLanguage != -1 && selectLanguage < languages.size()) {
            Language language = languages.get(selectLanguage);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KeyData.SELECT_LANGUAGE, language.getValue());
            editor.commit();
        }
    }

}
