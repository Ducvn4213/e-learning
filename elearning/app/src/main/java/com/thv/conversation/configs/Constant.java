package com.thv.conversation.configs;

/**
 * Created by NGUYENHUONG on 12/6/16.
 */

public class Constant {
    public static String LANGUAGE_SELECT = "vi";
    public static int TIME_NOTIFICATION_DEFAULT = 20;
    public static final String SERVER = "http://beelearn.org/elearning/api/";

    public static final String URL_FACEBOOK = "https://www.facebook.com/sovio.net/?fref=ts";
    public static final String URL_GROUP_FACEBOOK = "https://www.facebook.com/groups/982274605194296/";
    public static final String URL_REVIEW = "https://play.google.com/store/apps/details?id=com.thv.conversation";
    public static final String URL_OTHER_APP = "https://play.google.com/store/apps/developer?id=Sovio";
}
