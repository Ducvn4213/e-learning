package com.thv.conversation.yourWordActivity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.thv.conversation.MyActivity;
import com.thv.conversation.R;
import com.thv.conversation.gameActivity.GameActivity;
import com.thv.conversation.util.adapters.VocabularyAdapter;
import com.thv.conversation.util.dialog.DialogAddWord;
import com.thv.conversation.util.dialog.DialogMessage;
import com.thv.conversation.util.dialog.HandlerClickButton;
import com.thv.conversation.util.handle.HandleAudio;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public class YourWordActivity extends MyActivity implements YourWordInterfaces,
        VocabularyAdapter.OnClickListener, View.OnClickListener, DialogAddWord.OnclickActionPositive {
    private static final int RESULT_SPEECH = 102;
    private static final int MAX_RESULT = 3;
    private RecyclerView rvLessonDetail;
    private List<LessonDetail> listLessonDetail;
    private VocabularyAdapter lessonDetailAdapter;
    private YourWordPresenter mPresenter;
    private LessonDetail lessonDetailCurent;
    private TextView tvNoBookmark;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_word);
        setActionbar();
        bindingControl();
        mPresenter = new YourWordPresenter(this);
        mPresenter.getAllWordBookmark();
    }

    void setActionbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.textMessage));
        toolbar.setTitle(R.string.your_words);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.delete_bookmark, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_delete_bookmark:
                showDialogConfirmDelete();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_play_game:
                startActivityPlayGame();
                break;
            case R.id.btn_add_word:
                showDialogAddWord();
                break;
        }
    }

    public void showDialogAddWord() {
        DialogAddWord dialogAddWord = new DialogAddWord(YourWordActivity.this, this);
        dialogAddWord.showDialog();
    }

    public void showDialogConfirmDelete() {
        DialogMessage dialogMessage = new DialogMessage(YourWordActivity.this);
        dialogMessage.setContentMessage(getString(R.string.confirm_remove_bookmark));
        dialogMessage.setActionPositive(new HandlerClickButton() {
            @Override
            public void clickButton() {
                removeAllBookMark();
            }
        });
        dialogMessage.showDialog();
    }

    private void removeAllBookMark() {
        mPresenter.removeAllBookmark();
        listLessonDetail.clear();
        updateViewAdapter();
    }

    private void bindingControl() {
        rvLessonDetail = (RecyclerView) findViewById(R.id.recyclerview_lessson_detail);
        Button btnAddWord = (Button) findViewById(R.id.btn_add_word);
        Button btnPlayGame = (Button) findViewById(R.id.btn_play_game);
        tvNoBookmark = (TextView) findViewById(R.id.tv_no_bookmark);
        btnAddWord.setOnClickListener(this);
        btnPlayGame.setOnClickListener(this);
    }

    public void setupUI() {
        rvLessonDetail.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvLessonDetail.setLayoutManager(mLayoutManager);
        lessonDetailAdapter = new VocabularyAdapter(this, listLessonDetail);
        rvLessonDetail.setAdapter(lessonDetailAdapter);
        updateViewAdapter();
    }

    public void updateViewAdapter() {
        lessonDetailAdapter.notifyDataSetChanged();
        if (listLessonDetail.size() == 0) {
            tvNoBookmark.setVisibility(View.VISIBLE);
        } else {
            tvNoBookmark.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {
                    List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    mPresenter.checkCorrectAnswer(lessonDetailCurent, results);
                    updateViewAdapter();
                }
                break;
            }

        }
    }

    @Override
    public void finish() {
        setResult(Activity.RESULT_OK, getIntent());
        super.finish();
    }


    @Override
    public void onClickRecord(int position) {
        String language = "en-US";
        lessonDetailCurent = listLessonDetail.get(position);
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, language);
        intent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, language);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, language);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language);
        intent.putExtra(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES, language);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, lessonDetailCurent.getTitle());
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, MAX_RESULT);
        try {
            startActivityForResult(intent, RESULT_SPEECH);
        } catch (ActivityNotFoundException a) {
            Toast t = Toast.makeText(getApplicationContext(), R.string.devices_dont_support, Toast.LENGTH_SHORT);
            t.show();
        }

    }

    @Override
    public void getAllWordBookmark(List<LessonDetail> lessonDetails) {
        listLessonDetail = lessonDetails;
        setupUI();
    }

    @Override
    public void onClickAudioPlay(int position) {
        LessonDetail lessonDetail = listLessonDetail.get(position);
        HandleAudio.getIns().playMedia(lessonDetail.getAudio(), null);
    }

    @Override
    public void onClickBookmark(int position) {
        LessonDetail lessonDetail = listLessonDetail.get(position);
        if (lessonDetail.isBookMarkWord()) {
            lessonDetail.removeBookMark();
            mPresenter.removeBookmarkVocabulary(lessonDetail);
            listLessonDetail.remove(position);
        } else {
            lessonDetail.bookMarkLesson();
            mPresenter.bookmarkVocabulary(lessonDetail);
        }
        updateViewAdapter();
    }

    private void startActivityPlayGame() {
        if (listLessonDetail.size() < 6) {
            Toast.makeText(YourWordActivity.this, R.string.message_can_not_play_game, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(YourWordActivity.this, GameActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean saveWord(String word, String meaning) {
        LessonDetail lessonDetail = mPresenter.saveNewWord(word, meaning);
        if (lessonDetail == null) return false;
        listLessonDetail.add(0, lessonDetail);
        updateViewAdapter();
        return true;
    }
}
