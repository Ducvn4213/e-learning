package com.thv.conversation.tab3;

import android.content.Context;

import com.thv.conversation.util.objects.Lesson;

import java.util.List;

public class Tab3Presenter {
    private Tab3Interfaces mView;
    private Tab3Model mModel;
    private List<Lesson> lessons;

    public Tab3Presenter(Context context, Tab3Interfaces mView) {
        this.mView = mView;
        mModel = new Tab3Model(context);
    }


    public void getLessons() {
        lessons = mModel.loadDataCourse();
        mView.getDataLessonFinish(lessons);
    }


    void checkUnlockLesson(Lesson lesson) {
        boolean ischeckPass = false;
        if (lessons != null && lesson.isPassLesson()) {
            for (int i = 0; i < lessons.size(); i++) {
                if (lessons.get(i).getId() == lesson.getId()) {
                    if (!lessons.get(i).isPassLesson()) {
                        ischeckPass = true;
                    } else {
                        break;
                    }
                } else {
                    if (ischeckPass) {
                        if (!lessons.get(i).isOpen()) {
                            lessons.get(i).setUnlock(1);
                            mModel.unLockLesson(lesson);
                            break;
                        }
                    }
                }

            }
        }
    }
}
