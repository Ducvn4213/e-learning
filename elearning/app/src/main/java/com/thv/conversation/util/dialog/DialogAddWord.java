package com.thv.conversation.util.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.thv.conversation.R;

/**
 * Created by NGUYENHUONG on 12/19/16.
 */

public class DialogAddWord implements View.OnClickListener {

    private Context mContext;
    private AlertDialog alertDialog;
    private TextView tvActionPositive, tvActionNegative;
    private EditText edYourWord, edMeaning;
    private OnclickActionPositive onclickActionPositive;

    public DialogAddWord(Context context, OnclickActionPositive onclickActionPositive) {
        this.mContext = context;
        this.onclickActionPositive = onclickActionPositive;
        LayoutInflater _inflater = LayoutInflater.from(mContext);
        View view = _inflater.inflate(R.layout.dialog_add_word, null);
        tvActionPositive = (TextView) view.findViewById(R.id.tv_action_positive);
        tvActionNegative = (TextView) view.findViewById(R.id.tv_action_negative);
        edYourWord = (EditText) view.findViewById(R.id.ed_your_word);
        edMeaning = (EditText) view.findViewById(R.id.ed_meaning);
        tvActionPositive.setOnClickListener(this);
        tvActionNegative.setOnClickListener(this);
        AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
        dialog.setView(view);
        alertDialog = dialog.create();
    }

    public void showDialog() {
        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_action_negative:
                alertDialog.dismiss();
                break;
            case R.id.tv_action_positive:
                if (onclickActionPositive != null) {
                    String word = edYourWord.getText().toString();
                    String meaning = edMeaning.getText().toString();
                    if (onclickActionPositive.saveWord(word, meaning)) {
                        alertDialog.dismiss();
                    }
                }
                break;
        }
    }

    public interface OnclickActionPositive {
        boolean saveWord(String word, String meaning);
    }
}
