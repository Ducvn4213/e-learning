package com.thv.conversation.languageActivity;

import com.thv.conversation.util.objects.Language;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/26/16.
 */

public class LanguageInterface {
    interface RequireViewOps {
        void loadDataFinish(List<Language> languages, int currentSelect);

        void resetApplication();

        void finishActivity();
    }

    interface RequirePresenterOps {
        void loadDataFinish(List<Language> languages, int currentSelect);
    }
}
