package com.thv.conversation.mainActivity;

public class MainInterfaces {
    /**
     * Required View methods available to Presenter. A passive layer, responsible to show data and
     * receive user interactions
     */
    interface RequiredViewOps {
        void loadDataFinish(boolean isSuccess);
    }

    /**
     * Operations offered to View to communicate with Presenter. Processes user interactions, sends
     * data requests to Model, etc.
     */
    interface ProvidedPresenterOps {
    }

    /**
     * Required Presenter methods available to Model.
     */
    interface RequiredPresenterOps {
        void loadDataFinish(boolean isSuccess);
    }

    /**
     * Operations offered to Model to communicate with Presenter. Handles all data business logic.
     */
    interface ProvidedModelOps {

    }
}
