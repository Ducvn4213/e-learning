package com.thv.conversation.util.handle;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by NGUYENHUONG on 12/7/16.
 */

public class HandleImage {

    public static void showImage(String url, ImageView imageView, int imageHolder) {
        if (url != null && !url.equals("")) {
            Picasso.with(imageView.getContext()).load(url).placeholder(imageHolder).into(imageView);
        } else {
            Picasso.with(imageView.getContext()).load(imageHolder).into(imageView);
        }
    }
}
