package com.thv.conversation.tab3;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thv.conversation.R;
import com.thv.conversation.configs.KeyData;
import com.thv.conversation.phraseActivity.PhraseActivity;
import com.thv.conversation.util.adapters.LessonAdapter;
import com.thv.conversation.util.dialog.DialogMessage;
import com.thv.conversation.util.objects.Lesson;

import java.util.List;

public class Tab3 extends Fragment implements Tab3Interfaces, LessonAdapter.OnItemClickListener {
    private static final int PHRASE_ACTIVITY = 3;
    private Tab3Presenter tab3Presenter;
    private RecyclerView rvLessson;
    private LessonAdapter phraseAdapter;
    private List<Lesson> lessonsPhrase;
    private DialogMessage dialogMessage;
    Lesson lesson;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_1, container, false);
        bindingControl(view);
        if (lessonsPhrase == null) {
            getLesson();
        } else {
            setupUI();
        }
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void bindingControl(View view) {
        rvLessson = (RecyclerView) view.findViewById(R.id.recyclerview_lessson);
    }

    public void updateFromActivity() {
        if (getView() != null) {
            getLesson();
        }
    }

    public void getLesson() {
        tab3Presenter = new Tab3Presenter(getActivity(), this);
        tab3Presenter.getLessons();
    }


    public void setupUI() {
        rvLessson.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvLessson.setLayoutManager(mLayoutManager);
        phraseAdapter = new LessonAdapter(lessonsPhrase, this);
        rvLessson.setAdapter(phraseAdapter);
    }

    @Override
    public void getDataLessonFinish(List<Lesson> lessons) {
        this.lessonsPhrase = lessons;
        setupUI();
    }


    @Override
    public void onItemClick(int position) {
        lesson = lessonsPhrase.get(position);
        if (lesson.isOpen()) {
            Intent intent = new Intent(getActivity(), PhraseActivity.class);
            intent.putExtra(KeyData.LESSON_DATA, lesson);
            startActivityForResult(intent, PHRASE_ACTIVITY);
        } else {
            showUnlockLesson(position);
        }
    }


    public void showUnlockLesson(int position) {
        dialogMessage = new DialogMessage(getActivity());
        String contentMessage = String.format(getString(R.string.message_unlock), position);
        dialogMessage.setContentMessage(contentMessage);
        dialogMessage.showDialog();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHRASE_ACTIVITY && data.hasExtra(KeyData.LESSON_DATA)) {
            Lesson lessonData = (Lesson) data.getSerializableExtra(KeyData.LESSON_DATA);
            tab3Presenter.checkUnlockLesson(lessonData);
            lesson.setCurrentStar(lessonData.getCurrentStar());
            phraseAdapter.notifyDataSetChanged();
        }
    }
}
