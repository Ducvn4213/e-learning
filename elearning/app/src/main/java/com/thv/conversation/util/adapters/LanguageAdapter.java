package com.thv.conversation.util.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thv.conversation.R;
import com.thv.conversation.util.objects.Language;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/29/16.
 */

public class LanguageAdapter extends ArrayAdapter<Language> {
    private Context context;
    private List<Language> languages;
    private int currentLanguageSelect = -1;

    public LanguageAdapter(Context context, List<Language> languages, int currentSelect) {
        super(context, R.layout.item_language);
        this.context = context;
        this.languages = languages;
        this.currentLanguageSelect = currentSelect;
    }

    private static class ViewHolder {
        TextView tvLanguage;
        ImageView ivSelectLanguage;
        View viewSelectLanguage;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_language, parent, false);
            viewHolder.tvLanguage = (TextView) convertView.findViewById(R.id.tv_language);
            viewHolder.ivSelectLanguage = (ImageView) convertView.findViewById(R.id.iv_select_language);
            viewHolder.viewSelectLanguage = convertView.findViewById(R.id.view_select_language);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final Language language = languages.get(position);
        viewHolder.tvLanguage.setText(language.getLanguage());
        if (currentLanguageSelect == position) {
            viewHolder.ivSelectLanguage.setVisibility(View.VISIBLE);
        } else {
            viewHolder.ivSelectLanguage.setVisibility(View.INVISIBLE);
        }
        viewHolder.viewSelectLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentLanguageSelect = position;
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return languages.size();
    }

    public int getCurrentLanguageSelect() {
        return currentLanguageSelect;
    }
}