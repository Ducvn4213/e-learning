package com.thv.conversation.gameMatchWord;

import android.content.Context;

import com.thv.conversation.util.objects.ItemGame;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by NGUYENHUONG on 12/18/16.
 */

public class GameMatchWordPresenter {
    private static final int MAX_CAN_INCORECT = 3;
    private GameMatchWordModel mModel;
    private GameMatchWordInterfaces mView;
    private List<ItemGame> itemGames;
    private List<LessonDetail> lessonDetails;
    private int inCorrect = 0, score;

    GameMatchWordPresenter(Context context, GameMatchWordInterfaces mView) {
        this.mModel = new GameMatchWordModel(context);
        this.mView = mView;
    }

    void getAllWordBookMark() {
        lessonDetails = mModel.getAllWordBookMark();
        randomItemGames();
    }


    void getLessonDetails(Lesson lesson) {
        lessonDetails = mModel.getLessonDetail(lesson);
        randomItemGames();

    }

    public void resetGame() {
        inCorrect = 0;
        score = 0;
        randomItemGames();
        mView.updateStatusGame(score, MAX_CAN_INCORECT - inCorrect);
    }

    public void randomItemGames() {
        itemGames = new ArrayList<>();
        List<ItemGame> games = new ArrayList<>();
        int k = 1;
        for (int i = 0; i < lessonDetails.size(); i++) {
            if (!lessonDetails.get(i).getTitle().matches("[a-zA-Z ]*\\d+.*")) {
                LessonDetail lessonDetail = lessonDetails.get(i);
                games.add(new ItemGame(lessonDetail.getTitle(), lessonDetail.getMean()));
                games.add(new ItemGame(lessonDetail.getMean(), lessonDetail.getTitle()));
                if (i % 5 == 4) {
                    Collections.shuffle(games);
                    itemGames.addAll(games);
                    games.clear();
                }
                if (k++ > 10) break;
            }
        }
        Collections.shuffle(games);
        itemGames.addAll(games);
        mView.getAllLessonDetail(itemGames);
    }

    private int oldSelect = -1;

    void checkAnswer(int currentSelect) {
        if (currentSelect == oldSelect) {
            oldSelect = -1;
            itemGames.get(currentSelect).setSelected(false);
        } else if (oldSelect == -1) {
            itemGames.get(currentSelect).setSelected(true);
            oldSelect = currentSelect;
        } else {
            ItemGame oldLesson = itemGames.get(oldSelect);
            ItemGame currentLesson = itemGames.get(currentSelect);
            if (oldLesson.checkAnswer(currentLesson.getWord())) {
                score += 10;
                itemGames.remove(oldLesson);
                itemGames.remove(currentLesson);
                mView.playAudio(true);
            } else {
                oldLesson.setSelected(false);
                currentLesson.setSelected(false);
                mView.playAudio(false);
                inCorrect++;
            }
            oldSelect = -1;
            checkFinishGame();

        }
        mView.updateRecyclerView();
    }

    public void checkFinishGame() {

        if (itemGames.size() == 0 || inCorrect > MAX_CAN_INCORECT) {
            boolean isComplete = (itemGames.size() == 0);
            int highScore = mModel.getHighScore();
            if (highScore < score) {
                highScore = score;
                mModel.saveHighScore(highScore);
            }
            mView.finishGame(isComplete, score, highScore);
        } else {
            mView.updateStatusGame(score, MAX_CAN_INCORECT - inCorrect);
        }
    }
}
