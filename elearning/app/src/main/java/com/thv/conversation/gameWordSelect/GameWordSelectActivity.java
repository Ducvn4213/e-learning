package com.thv.conversation.gameWordSelect;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.thv.conversation.MyActivity;
import com.thv.conversation.R;
import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.dialog.DialogFinishGame;
import com.thv.conversation.util.handle.HandleAudio;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public class GameWordSelectActivity extends MyActivity implements GameWordSelectInterface, DialogFinishGame.OnClickButton, View.OnClickListener {
    ViewSwitcher vsContainGame;
    DialogFinishGame dialogFinishGame;
    GameWordSelectPresenter mPresenter;
    private TextView tvScore, tvIncorrect, tvWord, tvWord1, tvWord2, tvWord3, tvWord4, tvStatusCheck,
            tvWordCheck, tvMeanCheck, tvNextQuiz;
    private ImageView ivQuizIcon, ivQuizAnswer;
    private LessonDetail currentWord;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_word_select);
        bindingControl();
        mPresenter = new GameWordSelectPresenter(this, this);
        dialogFinishGame = new DialogFinishGame(this, this);
        if (getIntent().hasExtra(KeyData.LESSON_DATA)) {
            Lesson lesson = (Lesson) getIntent().getSerializableExtra(KeyData.LESSON_DATA);
            mPresenter.getLessonDetails(lesson);
        } else {
            mPresenter.getAllWordBookMark();
        }
    }

    private void bindingControl() {
        vsContainGame = (ViewSwitcher) findViewById(R.id.vs_contain_game);
        tvScore = (TextView) findViewById(R.id.tv_score);
        tvIncorrect = (TextView) findViewById(R.id.tv_incorrect);
        ivQuizIcon = (ImageView) findViewById(R.id.iv_quiz_icon);
        tvWord = (TextView) findViewById(R.id.tv_word);
        tvWord1 = (TextView) findViewById(R.id.tv_word_1);
        tvWord2 = (TextView) findViewById(R.id.tv_word_2);
        tvWord3 = (TextView) findViewById(R.id.tv_word_3);
        tvWord4 = (TextView) findViewById(R.id.tv_word_4);
        tvStatusCheck = (TextView) findViewById(R.id.tv_status_check);
        tvWordCheck = (TextView) findViewById(R.id.tv_word_check);
        tvMeanCheck = (TextView) findViewById(R.id.tv_mean_check);
        tvNextQuiz = (TextView) findViewById(R.id.tv_next_quiz);
        ImageView ivPlayAudio = (ImageView) findViewById(R.id.iv_play_audio);
        ivQuizAnswer = (ImageView) findViewById(R.id.iv_quiz_answer);
        ivPlayAudio.setOnClickListener(this);
        tvWord1.setOnClickListener(this);
        tvWord2.setOnClickListener(this);
        tvWord3.setOnClickListener(this);
        tvWord4.setOnClickListener(this);
        tvNextQuiz.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_play_audio:
                playAudioWord();
                break;
            case R.id.tv_word_1:
                mPresenter.checkAnswer(tvWord1.getText().toString());
                break;
            case R.id.tv_word_2:
                mPresenter.checkAnswer(tvWord2.getText().toString());
                break;
            case R.id.tv_word_3:
                mPresenter.checkAnswer(tvWord3.getText().toString());
                break;
            case R.id.tv_word_4:
                mPresenter.checkAnswer(tvWord4.getText().toString());
                break;
            case R.id.tv_next_quiz:
                nextQuestion();
                break;

        }
    }

    private void nextQuestion() {
        vsContainGame.showNext();
        mPresenter.nextWord();
    }

    private void playAudioWord() {
        if (currentWord != null) {
            HandleAudio.getIns().playMedia(currentWord.getAudio(), null);
        }
    }

    @Override
    public void updateView(LessonDetail lessonDetail, List<String> answers) {
        currentWord = lessonDetail;
        tvWord.setText(lessonDetail.getTitle());
        tvWord1.setText(answers.get(0));
        tvWord2.setText(answers.get(1));
        tvWord3.setText(answers.get(2));
        tvWord4.setText(answers.get(3));
    }

    @Override
    public void updateStatusGame(int score, int canInCorrect) {
        tvScore.setText(String.valueOf(score));
        tvIncorrect.setText(String.valueOf(canInCorrect));
        switch (canInCorrect) {
            case 0:
                ivQuizIcon.setImageResource(R.drawable.quiz_l1);
                break;
            case 1:
                ivQuizIcon.setImageResource(R.drawable.quiz_l2);
                break;
            case 2:
                ivQuizIcon.setImageResource(R.drawable.quiz_l3);
                break;
            case 3:
                ivQuizIcon.setImageResource(R.drawable.quiz_l4);
                break;
        }
    }

    @Override
    public void finishGame(boolean isSuccess, int score, int highScore) {
        dialogFinishGame.setContent(isSuccess, score, highScore);
        dialogFinishGame.showDialog();
    }

    @Override
    public void checkAnswerFinish(boolean isCorrect) {
        vsContainGame.showNext();
        if (isCorrect) {
            tvStatusCheck.setText(R.string.correct);
            ivQuizAnswer.setImageResource(R.drawable.quiz_r1);
            playMedia(R.raw.correct);
        } else {
            tvStatusCheck.setText(R.string.incorrect);
            ivQuizAnswer.setImageResource(R.drawable.quiz_w1);
            playMedia(R.raw.wrong);
        }
        tvWordCheck.setText(currentWord.getTitle());
        tvMeanCheck.setText(currentWord.getMean());
    }

    @Override
    public void onClickNegative() {
        finish();
    }

    @Override
    public void onClickPositive() {
        mPresenter.resetGame();
    }

    public void playMedia(int resource) {
        MediaPlayer playSoundNewMessage = MediaPlayer.create(GameWordSelectActivity.this, resource);
        playSoundNewMessage.start();
    }
}
