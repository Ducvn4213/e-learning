package com.thv.conversation.gameWordComplete;

import android.content.Context;

import com.thv.conversation.util.objects.ItemWordSelect;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public class GameWordCompletePresenter {
    private static final int MAX_CAN_INCORRECT = 3;
    private GameWordCompleteModel mModel;
    private GameWordCompleteInterface mView;
    private List<LessonDetail> lessonDetails;
    private int inCorrect = 0, score, currentQuestion = -1, currentSelectAnswer = 0;
    private List<ItemWordSelect> currentItemWordSelect;
    private String curentAnswer;

    GameWordCompletePresenter(Context context, GameWordCompleteInterface mView) {
        this.mModel = new GameWordCompleteModel(context);
        this.mView = mView;
    }

    void getAllWordBookMark() {
        lessonDetails = mModel.getAllWordBookMark();
        Collections.shuffle(lessonDetails);
        nextWord();
    }


    void getLessonDetails(Lesson lesson) {
        lessonDetails = mModel.getLessonDetail(lesson);
        Collections.shuffle(lessonDetails);
        nextWord();
    }

    void checkAnswer(int position) {
        if (currentItemWordSelect.get(position).getChar()
                == curentAnswer.charAt(currentSelectAnswer)) {
            currentItemWordSelect.get(position).setSelect(true);
            mView.checkAnswerFinish(true, currentItemWordSelect.get(position).getChar());
            currentSelectAnswer++;
        } else {
            inCorrect++;
            mView.checkAnswerFinish(false, currentItemWordSelect.get(position).getChar());
        }
        if (currentSelectAnswer == currentItemWordSelect.size()) {
            score += 10;
            inCorrect = 0;
            nextWord();
        }
        if (inCorrect > MAX_CAN_INCORRECT) {
            int highScore = mModel.getHighScore();
            if (highScore < score) {
                highScore = score;
                mModel.saveHighScore(highScore);
            }
            mView.finishGame(false, score, highScore);
        } else {
            mView.updateStatusGame(score, MAX_CAN_INCORRECT - inCorrect);
        }

    }

    void nextWord() {
        currentSelectAnswer = 0;
        currentItemWordSelect = new ArrayList<>();
        do {
            currentQuestion++;
        } while (lessonDetails.get(currentQuestion).getTitle().matches("[a-zA-Z ]*\\d+.*"));
        if (currentQuestion < lessonDetails.size()) {
            curentAnswer = lessonDetails.get(currentQuestion).getTitle().toUpperCase();
            for (int i = 0; i < curentAnswer.length(); i++) {
                currentItemWordSelect.add(new ItemWordSelect(curentAnswer.charAt(i)));
            }
            Collections.shuffle(currentItemWordSelect);
            mView.updateView(lessonDetails.get(currentQuestion), currentItemWordSelect);
        } else {
            int highScore = mModel.getHighScore();
            if (highScore < score) {
                highScore = score;
                mModel.saveHighScore(highScore);
            }
            mView.finishGame(true, score, highScore);
        }
    }


    void resetGame() {
        inCorrect = 0;
        score = 0;
        currentQuestion = -1;
        mView.updateStatusGame(score, MAX_CAN_INCORRECT - inCorrect);
        Collections.shuffle(lessonDetails);
        nextWord();
    }
}
