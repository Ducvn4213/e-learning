package com.thv.conversation.util.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.thv.conversation.R;

/**
 * Created by NGUYENHUONG on 12/22/16.
 */

public class DialogResult {
    private Context mContext;
    private AlertDialog dialogMessage;
    private TextView tvAverageScore;
    private TextView tvNumberCompleted;
    private TextView tvNumberInCompleted;

    public DialogResult(Context context) {
        this.mContext = context;
        LayoutInflater _inflater = LayoutInflater.from(mContext);
        View view = _inflater.inflate(R.layout.dialog_result, null);
        TextView tvActionPositive = (TextView) view.findViewById(R.id.tv_action_positive);
        tvAverageScore = (TextView) view.findViewById(R.id.tv_average_score);
        tvNumberCompleted = (TextView) view.findViewById(R.id.tv_number_completed);
        tvNumberInCompleted = (TextView) view.findViewById(R.id.tv_number_incompleted);
        AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
        dialog.setView(view);
        tvActionPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogMessage.dismiss();
            }
        });
        dialogMessage = dialog.create();
    }

    public void setResult(int averageScore, int mCompleted, int mInCompleted) {
        this.tvAverageScore.setText(String.valueOf(averageScore));
        this.tvNumberCompleted.setText(String.valueOf(mCompleted));
        this.tvNumberInCompleted.setText(String.valueOf(mInCompleted));
    }

    public void showDialog() {
        dialogMessage.show();
    }

}
