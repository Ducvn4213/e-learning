package com.thv.conversation.util.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thv.conversation.R;
import com.thv.conversation.util.objects.ItemWordSelect;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/21/16.
 */

public class GameWordCompleteAdapter extends RecyclerView.Adapter<GameWordCompleteAdapter.ViewHolder> {

    GameWordCompleteAdapter.OnClickListener onClickListener;
    List<ItemWordSelect> itemWorSelects;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCharacter;


        public ViewHolder(View view) {
            super(view);
            tvCharacter = (TextView) view.findViewById(R.id.tv_character_word);

        }

    }


    public GameWordCompleteAdapter(GameWordCompleteAdapter.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setItemWorSelects(List<ItemWordSelect> itemWorSelects) {
        this.itemWorSelects = itemWorSelects;
    }

    @Override
    public GameWordCompleteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_word_complete, parent, false);
        GameWordCompleteAdapter.ViewHolder vh = new GameWordCompleteAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(GameWordCompleteAdapter.ViewHolder holder, final int position) {
        if (itemWorSelects != null && itemWorSelects.size() > position) {
            if (!itemWorSelects.get(position).isSelect()) {
                holder.tvCharacter.setText(String.valueOf(itemWorSelects.get(position).getChar()));
                holder.tvCharacter.setVisibility(View.VISIBLE);
            } else {
                holder.tvCharacter.setVisibility(View.INVISIBLE);
            }

            holder.tvCharacter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClickItem(position);
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        if (itemWorSelects == null) return 0;
        return itemWorSelects.size();
    }

    public interface OnClickListener {

        void onClickItem(int position);
    }
}