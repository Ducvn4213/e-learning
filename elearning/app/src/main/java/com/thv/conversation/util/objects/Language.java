package com.thv.conversation.util.objects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NGUYENHUONG on 12/29/16.
 */

public class Language {
    @SerializedName("value")
    private String value;
    @SerializedName("language")
    private String language;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
