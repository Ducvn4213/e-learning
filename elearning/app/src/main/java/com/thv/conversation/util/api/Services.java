package com.thv.conversation.util.api;

import com.thv.conversation.configs.Constant;
import com.thv.conversation.util.objects.Language;
import com.thv.conversation.util.objects.Lesson;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;


public class Services {

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(Constant.SERVER).addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(1, TimeUnit.MINUTES);
        okHttpClient.setConnectTimeout(1, TimeUnit.MINUTES);
        Retrofit retrofit = builder.client(okHttpClient).build();
        return retrofit.create(serviceClass);
    }


    public static void getDataFromServer(Callback<List<Lesson>> callback, String languageCode) {
        API service = createService(API.class);
        Call<List<Lesson>> call = service.getDataFromServer("getdata", languageCode);
        call.enqueue(callback);
    }

    public static void getLanguageFromServer(Callback<List<Language>> callback) {
        API service = createService(API.class);
        Call<List<Language>> call = service.getLanguageFromServer("getlanguage");
        call.enqueue(callback);
    }
}
