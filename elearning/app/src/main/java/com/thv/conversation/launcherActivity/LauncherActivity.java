package com.thv.conversation.launcherActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.thv.conversation.R;
import com.thv.conversation.configs.Constant;
import com.thv.conversation.mainActivity.MainActivity;
import com.thv.conversation.settingActivity.AlarmReceiver;
import com.thv.conversation.settingActivity.SettingActivity;
import com.thv.conversation.util.objects.Language;

import java.util.Calendar;
import java.util.List;

/**
 * Created by duc.nv on 11/30/2016.
 */

public class LauncherActivity extends AppCompatActivity implements LauncherInterfaces.RequireViewOps {
    private AlertDialog alertDialog;
    private LauncherPresenter mPresenter;
    protected int _splashTime = 500;

    protected boolean _active = true;

    private Thread splashTread;
    private boolean isFinished;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        mPresenter = new LauncherPresenter(this);
        Constant.LANGUAGE_SELECT = mPresenter.getSelectLanguage();
        if (Constant.LANGUAGE_SELECT.equals("")) {
            mPresenter.getAllLanguage();
        } else {
            intializeUI();
        }

        enableReminder();
    }

    private void intializeUI() {
        splashTread = new Thread() {
            @Override
            public void run() {
                try {

                    int waited = 0;
                    while (_active && (waited < _splashTime)) {
                        sleep(100);
                        if (_active) {
                            waited += 100;
                        }
                    }
                } catch (InterruptedException e) {
                } finally {
                    startActivityMain();
                }
            }
        };
        splashTread.start();
    }

    public void startActivityMain() {
        Intent intent = new Intent(LauncherActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    private void enableReminder() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, SettingActivity.REQUEST_CODE, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 20);
        calendar.set(Calendar.MINUTE, 0);
        if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
            calendar.add(Calendar.DATE, 1);
        }
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 24 * 60 * 60 * 1000, broadcast);
    }

    @Override
    public void loadLanguageFinish(final List<Language> languages) {
        if (!isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(LauncherActivity.this);
            builder.setTitle(R.string.title_select_language);
            CharSequence[] items = new CharSequence[languages.size()];
            for (int i = 0; i < languages.size(); i++) {
                items[i] = languages.get(i).getLanguage();
            }
            builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    Constant.LANGUAGE_SELECT = languages.get(item).getValue();
                    mPresenter.saveSelectLanguage(Constant.LANGUAGE_SELECT);
                    alertDialog.dismiss();
                    startActivityMain();
                }
            });
            alertDialog = builder.create();
            alertDialog.setCancelable(false);

            alertDialog.show();
        }
    }

    @Override
    public void loadLanguageError() {
        Toast.makeText(LauncherActivity.this, R.string.error_connect_server, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        isFinished=true;
        super.onDestroy();
    }
}
