package com.thv.conversation.gameMatchWord;

import android.content.Context;
import android.content.SharedPreferences;

import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.database.SQLiteManager;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/18/16.
 */

public class GameMatchWordModel {
    SQLiteManager sqLiteManager;
    private SharedPreferences sharedPreferences;

    GameMatchWordModel(Context context) {
        sqLiteManager = new SQLiteManager(context);
        sharedPreferences = context.getSharedPreferences(KeyData.FILE_LOCAL, Context.MODE_PRIVATE);
    }

    List<LessonDetail> getAllWordBookMark() {
        return sqLiteManager.getAllWordBookmarkForGame();
    }

    List<LessonDetail> getLessonDetail(Lesson lesson) {
        return sqLiteManager.getLessonDetail(lesson.getId());
    }

    int getHighScore() {
        return sharedPreferences.getInt(KeyData.HIGH_SCORE, 0);
    }

    void saveHighScore(int highScore) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KeyData.HIGH_SCORE, highScore);
        editor.commit();
    }
}
