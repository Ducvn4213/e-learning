package com.thv.conversation.gameActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.thv.conversation.MyActivity;
import com.thv.conversation.R;
import com.thv.conversation.configs.KeyData;
import com.thv.conversation.gameMatchWord.GameMatchWordActivity;
import com.thv.conversation.gameWordComplete.GameWordCompleteActivity;
import com.thv.conversation.gameWordFlashcard.GameWordFlashCardActivity;
import com.thv.conversation.gameWordSelect.GameWordSelectActivity;
import com.thv.conversation.util.objects.Lesson;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public class GameActivity extends MyActivity implements View.OnClickListener {
    Lesson lesson;
    TextView tvWordSelect, tvWordFlashCard, tvWordComplete, tvMatchWord;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        bindingControl();
        if (getIntent().hasExtra(KeyData.LESSON_DATA)) {
            lesson = (Lesson) getIntent().getSerializableExtra(KeyData.LESSON_DATA);
        }
    }

    private void bindingControl() {
        tvWordSelect = (TextView) findViewById(R.id.tv_word_select);
        tvWordComplete = (TextView) findViewById(R.id.tv_word_complete);
        tvWordFlashCard = (TextView) findViewById(R.id.tv_word_flash_card);
        tvMatchWord = (TextView) findViewById(R.id.tv_match_word);
        tvWordSelect.setOnClickListener(this);
        tvWordComplete.setOnClickListener(this);
        tvWordFlashCard.setOnClickListener(this);
        tvMatchWord.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.tv_word_select:
                intent = new Intent(GameActivity.this, GameWordSelectActivity.class);
                break;
            case R.id.tv_word_complete:
                intent = new Intent(GameActivity.this, GameWordCompleteActivity.class);
                break;
            case R.id.tv_word_flash_card:
                intent = new Intent(GameActivity.this, GameWordFlashCardActivity.class);
                break;
            case R.id.tv_match_word:
                intent = new Intent(GameActivity.this, GameMatchWordActivity.class);
                break;
        }
        if (intent != null) {
            if (lesson != null) {
                intent.putExtra(KeyData.LESSON_DATA, lesson);
            }
            startActivity(intent);
        }
    }
}
