package com.thv.conversation.conversationActivity;

import android.content.Context;
import android.util.Log;

import com.thv.conversation.util.database.SQLiteManager;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public class ConversationModel {
    private SQLiteManager sqLiteManager;

    public ConversationModel(Context mContext) {
        sqLiteManager = new SQLiteManager(mContext);
    }

    public List<LessonDetail> getLessonDetail(Lesson lesson) {
        return sqLiteManager.getLessonDetail(lesson.getId());
    }

    public void saveLessonDetail(LessonDetail lessonDetail) {
        sqLiteManager.updateLessonDetail(lessonDetail);
    }
}
