package com.thv.conversation.util.objects;

/**
 * Created by NGUYENHUONG on 12/23/16.
 */

public class LessonStar {
    private int currentStar;
    private int totalStar;

    public LessonStar(int currentStar, int totalStar) {
        this.currentStar = currentStar;
        this.totalStar = totalStar;
    }

    public int getTotalStar() {
        return totalStar;
    }

    public void setTotalStar(int totalStar) {
        this.totalStar = totalStar;
    }

    public int getCurrentStar() {
        return currentStar;
    }

    public void setCurrentStar(int currentStar) {
        this.currentStar = currentStar;
    }
}
