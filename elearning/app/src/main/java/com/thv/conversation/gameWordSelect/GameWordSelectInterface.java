package com.thv.conversation.gameWordSelect;

import com.thv.conversation.util.objects.ItemGame;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public interface GameWordSelectInterface {
    void updateView(LessonDetail lessonDetail, List<String> listAnswers);

    void updateStatusGame(int score, int canInCorrect);

    void finishGame(boolean isSuccess, int score, int highScore);

    void checkAnswerFinish(boolean isCorrect);
}
