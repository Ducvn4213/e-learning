package com.thv.conversation.gameWordFlashcard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thv.conversation.MyActivity;
import com.thv.conversation.R;
import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.handle.HandleAudio;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public class GameWordFlashCardActivity extends MyActivity implements GameWordFlashCardInterface,
        View.OnClickListener, CardBackFragment.OnClickButton {
    private GameWordFlashCardPresenter mPresenter;
    private View viewCard, viewContain, viewProgressWordMaster,
            viewProgressWordReviewing, viewProgressWordLearning;
    private TextView tvStatusLearning, tvStatusReviewing, tvStatusMartered;
    private LessonDetail currentWord;
    private CardFrontFragment cardFrontFragment;
    private CardBackFragment cardBackFragment;
    private LinearLayout.LayoutParams layoutParamsLearning, layoutParamsReviewing, layoutParamsMastered;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_word_flash_card);
        bindingControl();
        mPresenter = new GameWordFlashCardPresenter(this, this);
        cardFrontFragment = new CardFrontFragment();
        cardBackFragment = new CardBackFragment();
        cardBackFragment.setOnClickButton(this);
        if (getIntent().hasExtra(KeyData.LESSON_DATA)) {
            Lesson lesson = (Lesson) getIntent().getSerializableExtra(KeyData.LESSON_DATA);
            mPresenter.getLessonDetails(lesson);
        } else {
            mPresenter.getAllWordBookMark();
        }
        if (savedInstanceState == null) {
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.view_card, cardFrontFragment)
                    .commit();
        }


    }


    private void bindingControl() {
        viewCard = findViewById(R.id.view_card);
        viewContain = findViewById(R.id.view_contain);
        viewProgressWordMaster = findViewById(R.id.view_progress_word_master);
        viewProgressWordReviewing = findViewById(R.id.view_progress_word_review);
        viewProgressWordLearning = findViewById(R.id.view_progress_word_learning);
        tvStatusLearning = (TextView) findViewById(R.id.tv_status_learning);
        tvStatusReviewing = (TextView) findViewById(R.id.tv_status_reviewing);
        tvStatusMartered = (TextView) findViewById(R.id.tv_status_mastered);
        layoutParamsLearning = (LinearLayout.LayoutParams) viewProgressWordLearning.getLayoutParams();
        layoutParamsReviewing = (LinearLayout.LayoutParams) viewProgressWordReviewing.getLayoutParams();
        layoutParamsMastered = (LinearLayout.LayoutParams) viewProgressWordMaster.getLayoutParams();

        viewCard.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_play_audio:
                playAudioWord();
                break;

            case R.id.view_card:
                flipCard();
                break;

        }
    }

    boolean mShowingBack = false;

    private void flipCard() {
        if (!mShowingBack) {

            mShowingBack = true;
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(
                            R.animator.card_flip_right_in,
                            R.animator.card_flip_right_out,
                            R.animator.card_flip_left_in,
                            R.animator.card_flip_left_out)
                    .replace(R.id.view_card, cardBackFragment)
                    .addToBackStack(null)
                    .commit();
            cardBackFragment.updateView(currentWord);
        }
    }

    private void playAudioWord() {
        if (currentWord != null) {
            HandleAudio.getIns().playMedia(currentWord.getAudio(), null);
        }
    }

    @Override
    public void updateView(LessonDetail lessonDetail) {
        currentWord = lessonDetail;
        cardFrontFragment.updateView(lessonDetail);
        if (mShowingBack) {
            mShowingBack = false;
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void updateStatusGame(final int countWordMaster, final int countWordMReviewing, final int countWordLearning, final int totalWord) {
        tvStatusLearning.setText(String.format(getString(R.string.learning_words), countWordLearning, totalWord));
        tvStatusReviewing.setText(String.format(getString(R.string.reviewing_words), countWordMReviewing, totalWord));
        tvStatusMartered.setText(String.format(getString(R.string.mastered_words), countWordMaster, totalWord));
        viewContain.postDelayed(new Runnable() {
            @Override
            public void run() {
                viewContain.invalidate();
                int width = viewContain.getWidth();
                int sizeLearning = width * countWordLearning / totalWord;
                int sizeReviewing = width * countWordMReviewing / totalWord;
                int sizeMastered = width * countWordMaster / totalWord;
                layoutParamsLearning.width = sizeLearning;
                layoutParamsReviewing.width = sizeReviewing;
                layoutParamsMastered.width = sizeMastered;
                viewProgressWordLearning.setLayoutParams(layoutParamsLearning);
                viewProgressWordReviewing.setLayoutParams(layoutParamsReviewing);
                viewProgressWordMaster.setLayoutParams(layoutParamsMastered);
            }
        }, 1);
    }

    @Override
    public void clickKnowWord() {
        mPresenter.answerWord(true);
    }

    @Override
    public void clickDontKnowWord() {
        mPresenter.answerWord(false);
    }
}
