package com.thv.conversation.phraseActivity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.thv.conversation.MyActivity;
import com.thv.conversation.R;
import com.thv.conversation.configs.KeyData;
import com.thv.conversation.util.adapters.PhraseAdapter;
import com.thv.conversation.util.dialog.DialogResult;
import com.thv.conversation.util.handle.HandleAudio;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public class PhraseActivity extends MyActivity implements PhraseInterfaces,
        PhraseAdapter.OnClickListener {
    private static final int RESULT_SPEECH = 102;
    private static final int MAX_RESULT = 3;
    private RecyclerView rvLessonDetail;
    private List<LessonDetail> listLessonDetail;
    private PhraseAdapter phraseAdapter;
    private Lesson lesson;
    private PhrasePresenter mPresenter;
    private LessonDetail phraseCurent;
    private DialogResult dialogResult;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phrase);
        lesson = (Lesson) getIntent().getSerializableExtra(KeyData.LESSON_DATA);
        setActionbar();
        bindingControl();
        mPresenter = new PhrasePresenter(this);
        mPresenter.getLessonDetail(lesson);
        dialogResult = new DialogResult(PhraseActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    void setActionbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        toolbar.setTitle(lesson.getTitle());
        toolbar.setTitleTextColor(getResources().getColor(R.color.textMessage));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chart, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.view_chart:
                showDialogResult();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void bindingControl() {
        rvLessonDetail = (RecyclerView) findViewById(R.id.recyclerview_lessson_detail);
    }

    public void setupUI() {
        rvLessonDetail.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvLessonDetail.setLayoutManager(mLayoutManager);
        phraseAdapter = new PhraseAdapter(this, listLessonDetail);
        rvLessonDetail.setAdapter(phraseAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {
                    List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    int star = mPresenter.checkCorrectAnswer(phraseCurent, results);
                    lesson.increaseCurrentStar(star);
                    phraseAdapter.notifyDataSetChanged();
                }

                break;
            }

        }
    }

    @Override
    public void finish() {
        HandleAudio.getIns().stopMediaPlayer();
        getIntent().putExtra(KeyData.LESSON_DATA, lesson);
        setResult(Activity.RESULT_OK, getIntent());
        super.finish();
    }


    @Override
    public void onClickRecord(int position) {
        String language = "en-US";
        phraseCurent = listLessonDetail.get(position);
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, language);
        intent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, language);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, language);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language);
        intent.putExtra(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES, language);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, phraseCurent.getTitle());
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, MAX_RESULT);
        try {
            startActivityForResult(intent, RESULT_SPEECH);
        } catch (ActivityNotFoundException a) {
            Toast t = Toast.makeText(getApplicationContext(), R.string.devices_dont_support, Toast.LENGTH_SHORT);
            t.show();
        }

    }

    @Override
    public void getLessonsDetailFinish(List<LessonDetail> lessonDetails) {
        listLessonDetail = lessonDetails;
        setupUI();
    }

    @Override
    public void onClickAudioPlay(int position) {
        LessonDetail lessonDetail = listLessonDetail.get(position);
        HandleAudio.getIns().playMedia(lessonDetail.getAudio(), null);
    }

    @Override
    public void onClickAudioPlaySlow(int position) {
        LessonDetail lessonDetail = listLessonDetail.get(position);
        HandleAudio.getIns().playMedia(lessonDetail.getAudioSlow(), null);
    }

    public void showDialogResult() {
        int averageScore = 0;
        int mCompleted = 0;
        int mInCompleted = 0;
        if (listLessonDetail.size() > 0) {
            for (int i = 0; i < listLessonDetail.size(); i++) {
                averageScore += listLessonDetail.get(i).getStar();
                if (listLessonDetail.get(i).getStar() > 0) {
                    mCompleted++;
                } else {
                    mInCompleted++;
                }
            }
            averageScore = averageScore / listLessonDetail.size();
        }
        dialogResult.setResult(averageScore, mCompleted, mInCompleted);
        dialogResult.showDialog();
    }
}
