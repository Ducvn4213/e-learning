package com.thv.conversation.util.api;

import com.thv.conversation.util.objects.Language;
import com.thv.conversation.util.objects.Lesson;

import java.util.List;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;


public interface API {

    @FormUrlEncoded
    @POST("_api.php")
    Call<List<Lesson>> getDataFromServer(@Field("_request") String request,
                                         @Field("_lang") String languageCode);

    @FormUrlEncoded
    @POST("_api.php")
    Call<List<Language>> getLanguageFromServer(@Field("_request") String request);
}
