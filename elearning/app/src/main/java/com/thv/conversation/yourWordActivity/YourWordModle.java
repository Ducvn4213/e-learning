package com.thv.conversation.yourWordActivity;

import android.content.Context;

import com.thv.conversation.util.database.SQLiteManager;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public class YourWordModle {
    Context mContext;
    SQLiteManager sqLiteManager;

    public YourWordModle(Context mContext) {
        this.mContext = mContext;
        sqLiteManager = new SQLiteManager(mContext);
    }

    public List<LessonDetail> getAllWordBookmark() {
        return sqLiteManager.getAllWordBookmark();
    }

    public void saveLessonDetail(LessonDetail lessonDetail) {
        sqLiteManager.updateLessonDetail(lessonDetail);
    }

    public void bookmarkVocabulary(LessonDetail lessonDetail) {
        sqLiteManager.saveBookmarLessonDetail(lessonDetail);
    }

    public void removeAllBookmark() {
        sqLiteManager.removeAllBookmark();
    }

    public int saveNewWord(LessonDetail lessonDetail) {
        return sqLiteManager.insertNewWord(lessonDetail);
    }
}
