package com.thv.conversation.util.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.thv.conversation.configs.TypeLesson;
import com.thv.conversation.configs.TypeStatus;
import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;
import com.thv.conversation.util.objects.LessonStar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by NGUYENHUONG on 12/6/16.
 */

public class SQLiteManager extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ENGLISH_SPEAKING_SQL";
    private static final String LESSON_TABLE = "lessons";
    private static final String DETAIL_TABLE = "details";
    private static final String KEY_ID = "id";
    private static final String KEY_TYPE = "type";
    private static final String KEY_TITLE = "title";
    private static final String KEY_AUDIO = "audio";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_UNLOCK = "unlock";   //0 or 1 - mở khóa hay chưa
    private static final String KEY_LANGUAGE = "language";//id language


    private static final String KEY_LESSONID = "lesson_id";
    private static final String KEY_MEANING = "meaning";
    private static final String KEY_AUDIO_2 = "audio_2";
    private static final String KEY_STAR = "star";
    private static final String KEY_TIME_SAVE = "timesave";
    private static final String KEY_STATUS = "status";

    private static final String[] COLUMNS_LESSON = {KEY_ID, KEY_TYPE, KEY_TITLE, KEY_AUDIO, KEY_IMAGE, KEY_UNLOCK, KEY_LANGUAGE};
    private static final String[] COLUMNS_DETAIL = {KEY_ID, KEY_LESSONID, KEY_TITLE, KEY_MEANING, KEY_AUDIO, KEY_AUDIO_2,
            KEY_STAR, KEY_TIME_SAVE, KEY_STATUS};

    public SQLiteManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public boolean checkTableExists() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(DATABASE_NAME, new String[]{"count(*)"}, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            if (cursor.getInt(0) > 0) {
                return true;
            }
        }
        return false;
    }

    public void deleteDatabase() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + COLUMNS_LESSON + " WHERE 1");
        db.execSQL("DELETE FROM " + COLUMNS_DETAIL + " WHERE 1");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USER_TABLE = String.format("CREATE TABLE %s(%s INTEGER PRIMARY KEY, %s INTEGER, %s TEXT, %s TEXT, %s TEXT, %s INTEGER, %s TEXT)",
                LESSON_TABLE, KEY_ID, KEY_TYPE, KEY_TITLE, KEY_AUDIO, KEY_IMAGE, KEY_UNLOCK, KEY_LANGUAGE);
        db.execSQL(CREATE_USER_TABLE);
        String CREATE_WORD_TABLE = String.format(
                "CREATE TABLE %s( %s INTEGER PRIMARY KEY, %s INTEGER, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER, %s INTEGER)",
                DETAIL_TABLE, KEY_ID, KEY_LESSONID, KEY_TITLE, KEY_MEANING, KEY_AUDIO, KEY_AUDIO_2, KEY_STAR, KEY_TIME_SAVE, KEY_STATUS);
        db.execSQL(CREATE_WORD_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + LESSON_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DETAIL_TABLE);
        this.onCreate(db);
    }

    public boolean checkExitsLesson(String languageCode) {
        boolean isExitsLesson = false;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(LESSON_TABLE, COLUMNS_LESSON, KEY_LANGUAGE + "= ?",
                new String[]{String.valueOf(languageCode)}, null, null, null);
        if (cursor.getCount() > 0) {
            isExitsLesson = true;
        }
        db.close();
        return isExitsLesson;
    }

    public List<Lesson> getAllLesson(String languageCode, int typeLeson) {
        Map<Integer, LessonStar> lessonStarMap = getAllLessonStar();

        List<Lesson> lessons = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(LESSON_TABLE, COLUMNS_LESSON, KEY_LANGUAGE + "= ? and " + KEY_TYPE + " = ?",
                new String[]{languageCode, String.valueOf(typeLeson)}, null, null, null);
        Lesson lesson;
        if (cursor.moveToFirst()) {
            do {
                lesson = new Lesson(
                        cursor.getInt(0),       //id
                        cursor.getInt(1),       //type
                        cursor.getString(2),    //title
                        cursor.getString(3),    //audio
                        cursor.getString(4),    //image
                        cursor.getInt(5),     //unlock
                        cursor.getString(6)     //idlanguage
                );
                if (lessonStarMap.containsKey(lesson.getId())) {
                    LessonStar star = lessonStarMap.get(lesson.getId());
                    lesson.setCurrentStar(star.getCurrentStar());
                    lesson.setTotalStar(star.getTotalStar());
                }
                lessons.add(lesson);
            } while (cursor.moveToNext());
        }
        db.close();
        return lessons;
    }

    public Map<Integer, LessonStar> getAllLessonStar() {

        Map<Integer, LessonStar> lesssonStars = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        int lessonID, totalStar, currentStar;
        Cursor cursor1 = db.query(DETAIL_TABLE, new String[]{KEY_LESSONID, "count(*)", "sum(" + KEY_STAR + ")"}, null,
                null, KEY_LESSONID, null, null);
        if (cursor1.moveToFirst()) {
            do {
                lessonID = cursor1.getInt(0);
                totalStar = cursor1.getInt(1) * 5;
                currentStar = cursor1.getInt(2);
                LessonStar lessonStar = new LessonStar(currentStar, totalStar);
                lesssonStars.put(lessonID, lessonStar);
            } while (cursor1.moveToNext());
        }
        return lesssonStars;
    }


    public List<LessonDetail> getLessonDetail(int idLessonDetail) {
        List<LessonDetail> lessonDetails = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(DETAIL_TABLE, COLUMNS_DETAIL, KEY_LESSONID + " = ? ",
                new String[]{String.valueOf(idLessonDetail)}, null, null, KEY_ID + " ASC");
        LessonDetail lessonDetail;
        if (cursor.moveToFirst()) {
            do {
                lessonDetail = new LessonDetail(
                        cursor.getInt(0),       //id
                        cursor.getInt(1),       //id_lesson
                        cursor.getString(2),    //title
                        cursor.getString(3),    //mean
                        cursor.getString(4),    //audio
                        cursor.getString(5),    //audio_2
                        cursor.getInt(6),       //star
                        cursor.getInt(7),        //timesave)
                        cursor.getInt(8)        //status)
                );
                lessonDetails.add(lessonDetail);
            } while (cursor.moveToNext());
        }
        db.close();
        return lessonDetails;
    }

    public List<LessonDetail> getAllWordBookmark() {
        List<LessonDetail> lessonDetails = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(DETAIL_TABLE, COLUMNS_DETAIL, KEY_TIME_SAVE + " > 0 ",
                null, null, null, KEY_TIME_SAVE + " DESC");
        LessonDetail lessonDetail;
        if (cursor.moveToFirst()) {
            do {
                lessonDetail = new LessonDetail(
                        cursor.getInt(0),       //id
                        cursor.getInt(1),       //id_lesson
                        cursor.getString(2),    //title
                        cursor.getString(3),    //mean
                        cursor.getString(4),    //audio
                        cursor.getString(5),    //audio_2
                        cursor.getInt(6),       //star
                        cursor.getInt(7),       //timesave)
                        cursor.getInt(8)        //timesave)
                );
                lessonDetails.add(lessonDetail);
            } while (cursor.moveToNext());
        }
        db.close();
        return lessonDetails;
    }

    public List<LessonDetail> getAllWordBookmarkForGame() {
        List<LessonDetail> lessonDetails = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(DETAIL_TABLE, COLUMNS_DETAIL, KEY_TIME_SAVE + " > 0 ",
                null, null, null, "RANDOM()", "20");
        LessonDetail lessonDetail;
        if (cursor.moveToFirst()) {
            do {
                lessonDetail = new LessonDetail(
                        cursor.getInt(0),       //id
                        cursor.getInt(1),       //id_lesson
                        cursor.getString(2),    //title
                        cursor.getString(3),    //mean
                        cursor.getString(4),    //audio
                        cursor.getString(5),    //audio_2
                        cursor.getInt(6),       //star
                        cursor.getInt(7),       //timesave)
                        cursor.getInt(8)        //timesave)
                );
                lessonDetails.add(lessonDetail);
            } while (cursor.moveToNext());
        }
        db.close();
        return lessonDetails;
    }

    public void insertMultiLesson(List<Lesson> lessons) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String sql = String.format("INSERT INTO %s (%s, %s, %s ,%s ,%s ,%s ,%s) VALUES (?, ?, ?, ?, ?, ?, ?)",
                    LESSON_TABLE, KEY_ID, KEY_TYPE, KEY_TITLE, KEY_AUDIO, KEY_IMAGE, KEY_UNLOCK, KEY_LANGUAGE);
            db.beginTransaction();
            SQLiteStatement stmt = db.compileStatement(sql);
            for (int i = 0; i < lessons.size(); i++) {
                Lesson lesson = lessons.get(i);
                stmt.bindString(1, String.valueOf(lesson.getId()));
                stmt.bindString(2, String.valueOf(lesson.getType()));
                stmt.bindString(3, lesson.getTitle());
                stmt.bindString(4, lesson.getAudio());
                stmt.bindString(5, lesson.getImage());
                stmt.bindString(6, String.valueOf(1));
                stmt.bindString(7, lesson.getIdLanguage());
                stmt.execute();
                stmt.clearBindings();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }

    public void insertMultiLessonDetail(List<LessonDetail> lessonDetails) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String sql = String.format("INSERT INTO %s (%s, %s ,%s ,%s ,%s ,%s ,%s) VALUES (?, ?, ?, ?, ?, ?, ?)",
                    DETAIL_TABLE, KEY_LESSONID, KEY_TITLE, KEY_MEANING, KEY_AUDIO, KEY_AUDIO_2, KEY_STAR, KEY_TIME_SAVE);
            db.beginTransaction();
            SQLiteStatement stmt = db.compileStatement(sql);
            for (int i = 0; i < lessonDetails.size(); i++) {
                LessonDetail lessonDetail = lessonDetails.get(i);
                stmt.bindString(1, String.valueOf(lessonDetail.getIdLesson()));
                stmt.bindString(2, lessonDetail.getTitle());
                stmt.bindString(3, lessonDetail.getMean());
                stmt.bindString(4, lessonDetail.getAudio());
                stmt.bindString(5, lessonDetail.getAudioSlow());
                stmt.bindString(6, String.valueOf(lessonDetail.getStar()));
                stmt.bindString(7, String.valueOf(lessonDetail.getTimeSave()));
                stmt.bindString(7, String.valueOf(TypeStatus.NORMAL));
                stmt.execute();
                stmt.clearBindings();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }


    public int insertNewWord(LessonDetail lessonDetail) {
        long index = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_LESSONID, String.valueOf(lessonDetail.getIdLesson()));
            values.put(KEY_TITLE, lessonDetail.getTitle());
            values.put(KEY_MEANING, lessonDetail.getMean());
            values.put(KEY_AUDIO, lessonDetail.getAudio());
            values.put(KEY_AUDIO_2, lessonDetail.getAudioSlow());
            values.put(KEY_STAR, String.valueOf(lessonDetail.getStar()));
            values.put(KEY_TIME_SAVE, String.valueOf(lessonDetail.getTimeSave()));
            index = db.insert(DETAIL_TABLE, null, values);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.close();
        }
        return (int) index;
    }

    public void unLockLesson(Lesson lesson) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_UNLOCK, lesson.getUnlock());
        db.update(LESSON_TABLE, values, KEY_ID + " = ?",
                new String[]{String.valueOf(lesson.getId())});
        db.close();
    }


    public void updateLessonDetail(LessonDetail lessonDetail) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_STAR, lessonDetail.getStar());
        values.put(KEY_STATUS, lessonDetail.getStatus());
        db.update(DETAIL_TABLE, values, KEY_ID + " = ?",
                new String[]{String.valueOf(lessonDetail.getId())});
        db.close();
    }

    public void saveBookmarLessonDetail(LessonDetail lessonDetail) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TIME_SAVE, lessonDetail.getTimeSave());
        db.update(DETAIL_TABLE, values, KEY_ID + " = ?",
                new String[]{String.valueOf(lessonDetail.getId())});
        db.close();
    }


    public void removeAllBookmark() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TIME_SAVE, 0);
        db.update(DETAIL_TABLE, values, "1", null);
        db.close();
    }
}