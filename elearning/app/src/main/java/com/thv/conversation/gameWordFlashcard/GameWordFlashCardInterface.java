package com.thv.conversation.gameWordFlashcard;

import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public interface GameWordFlashCardInterface {
    void updateView(LessonDetail lessonDetail);

    void updateStatusGame(int countWordMaster, int countWordMReviewing, int countWordMLearning, int totalWord);
}
