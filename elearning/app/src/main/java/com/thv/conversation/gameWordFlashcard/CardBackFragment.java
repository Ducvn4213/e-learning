package com.thv.conversation.gameWordFlashcard;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thv.conversation.R;
import com.thv.conversation.configs.TypeStatus;
import com.thv.conversation.util.handle.HandleAudio;
import com.thv.conversation.util.objects.LessonDetail;

/**
 * Created by NGUYENHUONG on 1/16/17.
 */

public class CardBackFragment extends Fragment implements View.OnClickListener {
    private TextView tvMeanWord, tvWord, tvStatus;
    private OnClickButton onClickButton;
    private LessonDetail lessonDetail;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_back_flash_card, container, false);
        bindingControls(view);
        if (lessonDetail != null) updateView();
        return view;
    }

    public void setOnClickButton(OnClickButton onClickButton) {
        this.onClickButton = onClickButton;
    }

    public void bindingControls(View view) {
        tvWord = (TextView) view.findViewById(R.id.tv_word);
        tvMeanWord = (TextView) view.findViewById(R.id.tv_mean_word);
        tvStatus = (TextView) view.findViewById(R.id.tv_status);
        ImageView ivPlayAudio = (ImageView) view.findViewById(R.id.iv_play_audio);

        TextView tvKnowWord = (TextView) view.findViewById(R.id.tv_know_word);
        TextView tvDontKnowWord = (TextView) view.findViewById(R.id.tv_dont_know_word);
        tvKnowWord.setOnClickListener(this);
        tvDontKnowWord.setOnClickListener(this);
        ivPlayAudio.setOnClickListener(this);
    }


    private void updateView() {
        if (tvMeanWord != null && isAdded()) {
            tvWord.setText(lessonDetail.getTitle());
            tvMeanWord.setText(lessonDetail.getMean());
            switch (lessonDetail.getStatus()) {
                case TypeStatus.LEARNING:
                    tvStatus.setText(R.string.learning);
                    tvStatus.setTextColor(getResources().getColor(R.color.colorViewLearningWord));
                    break;
                case TypeStatus.REVIEWING:
                    tvStatus.setText(R.string.reviewing);
                    tvStatus.setTextColor(getResources().getColor(R.color.colorViewReviewingWord));
                    break;
                case TypeStatus.MASTERED:
                    tvStatus.setText(R.string.mastered);
                    tvStatus.setTextColor(getResources().getColor(R.color.colorViewMasteredWord));
                    break;
            }
        }
    }


    public void updateView(LessonDetail lessonDetail) {
        this.lessonDetail = lessonDetail;
        updateView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_know_word:
                onClickButton.clickKnowWord();
                break;
            case R.id.tv_dont_know_word:
                onClickButton.clickDontKnowWord();
                break;
            case R.id.iv_play_audio:
                HandleAudio.getIns().playMedia(lessonDetail.getAudio(), null);
                break;
        }
    }

    public interface OnClickButton {
        void clickKnowWord();

        void clickDontKnowWord();
    }
}
