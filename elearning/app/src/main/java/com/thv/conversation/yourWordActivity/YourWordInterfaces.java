package com.thv.conversation.yourWordActivity;

import com.thv.conversation.util.objects.LessonDetail;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/8/16.
 */

public interface YourWordInterfaces {

    void getAllWordBookmark(List<LessonDetail> lessonDetails);
}
