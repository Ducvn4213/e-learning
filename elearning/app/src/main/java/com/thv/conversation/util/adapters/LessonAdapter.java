package com.thv.conversation.util.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thv.conversation.R;
import com.thv.conversation.util.handle.HandleImage;
import com.thv.conversation.util.objects.Lesson;

import java.util.List;

/**
 * Created by NGUYENHUONG on 12/7/16.
 */

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.ViewHolder> {
    private List<Lesson> mDataset;
    private OnItemClickListener listener;


    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitleLesson, tvNeedPass, tvCurrentStar;
        private ImageView ivPassLesson, ivLockLesson;
        private View viewLesson;

        ViewHolder(View view) {
            super(view);
            tvTitleLesson = (TextView) view.findViewById(R.id.tv_title_lesson);
            tvNeedPass = (TextView) view.findViewById(R.id.tv_need_pass);
            tvCurrentStar = (TextView) view.findViewById(R.id.tv_current_star);
            ivPassLesson = (ImageView) view.findViewById(R.id.iv_pass_lesson);
            ivLockLesson = (ImageView) view.findViewById(R.id.iv_lock_lesson);
            viewLesson = view.findViewById(R.id.view_lesson);
        }
    }

    public LessonAdapter(List<Lesson> myDataset, OnItemClickListener listener) {
        mDataset = myDataset;
        this.listener = listener;
    }

    @Override
    public LessonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lesson, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Lesson lesson = mDataset.get(position);
        holder.tvTitleLesson.setText(lesson.getTitle());
        if (lesson.isPassLesson()) {
            holder.ivPassLesson.setVisibility(View.VISIBLE);
        } else {
            holder.ivPassLesson.setVisibility(View.GONE);
        }
        if (lesson.isOpen()) {
            String messageNeedPass = String.format(holder.tvNeedPass.getContext().getString(R.string.message_need_pass), lesson.getNeedPass());
            holder.tvNeedPass.setText(messageNeedPass);
            holder.tvNeedPass.setVisibility(View.VISIBLE);
            holder.ivLockLesson.setVisibility(View.INVISIBLE);
        } else {
            holder.tvNeedPass.setVisibility(View.INVISIBLE);
            holder.ivLockLesson.setVisibility(View.VISIBLE);
        }
        holder.tvCurrentStar.setText(String.format("%d/%d", lesson.getCurrentStar(), lesson.getTotalStar()));
        holder.viewLesson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mDataset == null) return 0;
        return mDataset.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}

