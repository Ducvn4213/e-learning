package com.thv.conversation.launcherActivity;

import com.thv.conversation.util.objects.Language;

import java.util.List;

/**
 * Created by duc.nv on 11/30/2016.
 */

public class LauncherPresenter implements LauncherInterfaces.RequirePresenterOps {
    private LauncherInterfaces.RequireViewOps mView;
    private LauncherModel mModel;

    LauncherPresenter(LauncherActivity mView) {
        this.mView = mView;
        mModel = new LauncherModel(mView, this);
    }

    void saveSelectLanguage(String countryCode) {
        mModel.saveSelectLanguage(countryCode);
    }

    String getSelectLanguage() {
        return mModel.getSelectLanguage();
    }

    void getAllLanguage() {
        mModel.getAllLanguages();
    }

    @Override
    public void loadLanguageFinish(List<Language> languages) {
        mView.loadLanguageFinish(languages);
    }

    @Override
    public void loadLanguageError() {
        mView.loadLanguageError();
    }
}
