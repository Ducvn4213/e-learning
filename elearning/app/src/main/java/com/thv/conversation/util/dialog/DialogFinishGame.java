package com.thv.conversation.util.dialog;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.thv.conversation.R;

/**
 * Created by NGUYENHUONG on 12/22/16.
 */

public class DialogFinishGame implements View.OnClickListener {
    private Context mContext;
    private AlertDialog dialogMessage;
    private OnClickButton onClickButton;
    private TextView tvTitle, tvCurrentScore, tvHighScore;

    public DialogFinishGame(Context context, OnClickButton onClickButton) {
        this.mContext = context;
        this.onClickButton = onClickButton;
        LayoutInflater _inflater = LayoutInflater.from(mContext);
        View view = _inflater.inflate(R.layout.dialog_finish_game, null);
        Button btnPositive = (Button) view.findViewById(R.id.btn_positive);
        Button btnNegative = (Button) view.findViewById(R.id.btn_negative);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvCurrentScore = (TextView) view.findViewById(R.id.tv_current_score);
        tvHighScore = (TextView) view.findViewById(R.id.tv_high_score);
        btnPositive.setOnClickListener(this);
        btnNegative.setOnClickListener(this);
        AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
        dialog.setView(view);
        dialogMessage = dialog.create();
        dialogMessage.setCancelable(false);
    }

    public void setContent(boolean isSuccess, int currentScore, int highScore) {
        tvCurrentScore.setText(String.valueOf(currentScore));
        if (isSuccess) {
            tvTitle.setText(R.string.title_high_score);
            tvHighScore.setText(R.string.congratulation);
        } else {
            tvTitle.setText(R.string.title_game_over);
            String message = mContext.getResources().getString(R.string.message_high_score);
            tvHighScore.setText(String.format(message, highScore));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_positive:
                onClickButton.onClickPositive();
                break;
            case R.id.btn_negative:
                onClickButton.onClickNegative();
                break;
        }
        dialogMessage.dismiss();
    }

    public void showDialog() {
        dialogMessage.show();
    }


    public interface OnClickButton {
        void onClickNegative();

        void onClickPositive();
    }
}
