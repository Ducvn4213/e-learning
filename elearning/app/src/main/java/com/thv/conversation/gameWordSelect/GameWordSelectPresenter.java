package com.thv.conversation.gameWordSelect;

import android.content.Context;

import com.thv.conversation.util.objects.Lesson;
import com.thv.conversation.util.objects.LessonDetail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by NGUYENHUONG on 1/13/17.
 */

public class GameWordSelectPresenter {
    private static final int MAX_CAN_INCORECT = 3;
    private GameWordSelectModel mModel;
    private GameWordSelectInterface mView;
    private List<LessonDetail> lessonDetails;
    private int inCorrect = 0, score, currentSelect = -1;

    GameWordSelectPresenter(Context context, GameWordSelectInterface mView) {
        this.mModel = new GameWordSelectModel(context);
        this.mView = mView;
    }

    void getAllWordBookMark() {
        lessonDetails = mModel.getAllWordBookMark();
        updateData();
        nextWord();
    }


    void getLessonDetails(Lesson lesson) {
        lessonDetails = mModel.getLessonDetail(lesson);
        updateData();
        nextWord();
    }


    private void updateData() {
        for (int i = lessonDetails.size() - 1; i >= 0; i--) {
            if (lessonDetails.get(i).getTitle().matches("[a-zA-Z ]*\\d+.*")) {
                lessonDetails.remove(i);
            }
        }
        Collections.shuffle(lessonDetails);
    }

    void checkAnswer(String answer) {
        if (lessonDetails.get(currentSelect).getMean().equals(answer)) {
            mView.checkAnswerFinish(true);
            score++;
        } else {
            mView.checkAnswerFinish(false);
            inCorrect++;
        }
        if (inCorrect > MAX_CAN_INCORECT) {
            int highScore = mModel.getHighScore();
            if (highScore < score) {
                highScore = score;
                mModel.saveHighScore(highScore);
            }
            mView.finishGame(false, score, highScore);
        } else {
            mView.updateStatusGame(score, MAX_CAN_INCORECT - inCorrect);

        }
    }

    void nextWord() {
        currentSelect++;
        if (currentSelect < lessonDetails.size()) {
            List<String> listAnswers = new ArrayList<>();
            Random ran = new Random();
            List<Integer> arrContain = new ArrayList<>();
            arrContain.add(currentSelect);
            listAnswers.add(lessonDetails.get(currentSelect).getMean());
            while (arrContain.size() < 4) {
                int i = ran.nextInt(lessonDetails.size() - 1);
                if (!arrContain.contains(i)) {
                    arrContain.add(i);
                    listAnswers.add(lessonDetails.get(i).getMean());
                }
            }
            Collections.shuffle(listAnswers);
            mView.updateView(lessonDetails.get(currentSelect), listAnswers);
        } else {
            int highScore = mModel.getHighScore();
            if (highScore < score) {
                highScore = score;
                mModel.saveHighScore(highScore);
            }
            mView.finishGame(true, score, highScore);
        }
    }


    public void resetGame() {
        inCorrect = 0;
        score = 0;
        currentSelect = -1;
        mView.updateStatusGame(score, MAX_CAN_INCORECT - inCorrect);
        Collections.shuffle(lessonDetails);
        nextWord();
    }
}
